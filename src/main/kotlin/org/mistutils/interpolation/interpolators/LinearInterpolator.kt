package org.mistutils.interpolation.interpolators

import org.mistutils.interpolation.Interpolator


/**
 * Linear interpolation.
 */
class LinearInterpolator : Interpolator {

    override fun interpolate(value: Double): Double {
        return value
    }

    companion object {
        val IN_OUT = LinearInterpolator()
    }
}
