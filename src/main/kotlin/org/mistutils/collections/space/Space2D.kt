package org.mistutils.collections.space

import org.mistutils.geometry.rectangle.Rect
import java.util.ArrayList

/**
 * A data structure that stores objects with bounding areas at locations,
 * and allows queries for objects in an area or the closest object to a location.
 */
// TODO: reactive stream support?
interface Space2D<T: Located2D> {

    /**
     * Add a located object to this space.
     * If already added (at identical position), will do nothing.
     */
    fun add(located: T)

    /**
     * Remove a located object from this space, if present at the position.
     * @returns true if removed, false if not found.
     */
    fun remove(located: Located2D): Boolean

    /**
     * Called after the coordinates of the specified located have been changed.
     * Pass in the previous coordinates that were known to this Space2D.
     */
    fun onMoved(located: Located2D, oldX: Double, oldY: Double)

    /**
     * Called after the bounding size of the specified located has been changed.
     * Pass in the previous size that was known to this Space2D.
     */
    fun onBoundingSizeChanged(located: Located2D, oldSize: Double)

    /**
     * @area the area to visit intersecting objects in.
     * @param minSize minimum bounding size of objects to visit (inclusive).  Smaller objects will be skipped.  Defaults to zero.
     * @param maxSize maximum bounding size of objects to visit (inclusive).  Larger objects will be skipped.  Defaults to positive infinity.
     * @param visitor to call for each object.
     */
    fun visitObjects(area: Rect,
                     minSize: Double = 0.0,
                     maxSize: Double = Double.POSITIVE_INFINITY,
                     visitor: (T) -> Boolean)

    /**
     * Get N closest objects to the specified point.
     * @param x position
     * @param y position
     * @param maxNum number of points to get (result may be fewer if there are not that many items in the space)
     * @param minDistance minimum distance to look for closest points at
     * @param maxDistance maximum distance to look for closest points at
     * @param minSize minimum bounding size of objects to take into account (inclusive).  Smaller objects will be skipped.  Defaults to zero.
     * @param maxSize maximum bounding size of objects to take into account (inclusive).  Larger objects will be skipped.  Defaults to positive infinity.
     * @param objectFilter filter to apply to objects before including them in the distance calculations.  If it returns false, the object is skipped. If null, no filter is applied.
     * @param out list to set the objects and their distances to (existing content will be removed first).
     *        Pass in a reused list, or omit it to create a new ArrayList each time.
     * @returns list of objects and (squared) distances to them, sorted closest objects first.
     */
    fun findNClosest(x: Double,
                     y: Double,
                     maxNum: Int,
                     out: MutableList<LocationDistance<T>> = ArrayList(maxNum),
                     minDistance: Double = 0.0,
                     maxDistance: Double = Double.POSITIVE_INFINITY,
                     minSize: Double = 0.0,
                     maxSize: Double = Double.POSITIVE_INFINITY,
                     objectFilter: ((T) -> Boolean)? = null): MutableList<LocationDistance<T>>

    /**
     * @returns the distance to the closest existing object, or null if there are no existing objects.
     */
    fun distanceToClosest(x: Double, y: Double): Double? {
        return findNClosest(x, y, 1).firstOrNull()?.calculateDistance()
    }

    /**
     * @returns the squared distance to the closest existing object, or null if there are no existing objects.
     */
    fun distanceToClosestSquared(x: Double, y: Double): Double? {
        return findNClosest(x, y, 1).firstOrNull()?.distanceSquared
    }

}