package org.mistutils.collections.space

import org.mistutils.geometry.double2.Double2
import java.lang.IllegalArgumentException

/**
 * Something that can be located in a [Space2D].
 */
interface Located2D: Double2 {

    /**
     * x position in the space.
     */
    override var x: Double

    /**
     * y position in the space.
     */
    override var y: Double

    /**
     * A bounding diameter for this object.  All of the object should fall within this diameter if you want to be able to
     * find potentially intersecting large objects that are outside of a query rectangle.
     *
     * For best performance, a boundingSize close to the objects real size is recommended.
     * A boundingSize of zero is not recommended, but is supported.  However, the [HierarchicalSpace2D] data structure
     * will not be able to function optimally in that case.
     *
     * (May also be used as side length of an axis-aligned bounding square centered on the object, depending on implementation).
     */
    var boundingSize: Double

    /**
     * [Space2D] that this object is located in.  May be null if it is not currently located anywhere.
     */
    var space2D: Space2D<out Located2D>?


    /**
     * Update the position of this located, and also notify the space it is in about the change.
     */
    fun updatePosition(posX: Double, posY: Double) {
        if (x != posX && y != posY) {
            val oldX = x
            val oldY = y
            x = posX
            y = posY
            space2D?.onMoved(this, oldX, oldY)
        }
    }

    /**
     * Update bounding size of this located instance, and also notifies the space it is located in about the change.
     */
    fun updateBoundingSize(newBoundingSize: Double) {
        if (boundingSize != newBoundingSize) {
            val oldSize = boundingSize
            boundingSize = newBoundingSize
            space2D?.onBoundingSizeChanged(this, oldSize)
        }
    }

    /**
     * Change the space that this Located2D is in.  Removes it from the old space and adds it to the new space.
     * The position can also be updated at the same time.
     */
    fun <T: Located2D>updateSpace(newSpace: Space2D<T>?, posX: Double = x, posY: Double = y) {
        if (newSpace != space2D) {
            // Remove from old
            space2D?.remove(this)

            // Update pos
            x = posX
            y = posY

            // Update space
            space2D = newSpace

            // Add to new space
            newSpace?.add(this as? T ?: throw IllegalArgumentException("Expected the type T of the newSpace $newSpace to hold Located2D objects of this type, but it did not."))
        }
        else {
            // Space was the same, just update the position if needed
            updatePosition(posX, posY)
        }
    }

    /**
     * Call this when the position of this Located2D instance changes.
     * It notifies the Space2D it is located in of the change, allowing it to update it's spatial data structures.
     *
     * The old position of the object is required.
     */
    fun notifySpaceWhenMoved(oldX: Double, oldY: Double) {
        space2D?.onMoved(this, oldX, oldY)
    }

    /**
     * Call this when the boundingSize of this Located2D instance changes.
     * It notifies the Space2D it is located in of the change, allowing it to update it's spatial data structures.
     *
     * The old size of the object is required.
     */
    fun notifySpaceWhenBoundingSizeChanged(oldSize: Double) {
        space2D?.onBoundingSizeChanged(this, oldSize)
    }

    /**
     * Call this when this object is destroyed or removed from use.
     * Notifies the Space2D it is located about it, allowing it to remove this object from it.
     */
    fun notifySpaceWhenRemoved() {
        space2D?.remove(this)
    }


}