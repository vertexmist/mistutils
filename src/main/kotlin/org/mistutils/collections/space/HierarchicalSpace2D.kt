package org.mistutils.collections.space

import com.koloboke.collect.map.IntObjMap
import com.koloboke.collect.map.hash.HashIntObjMaps
import com.koloboke.function.IntObjConsumer
import org.mistutils.checking.Check
import org.mistutils.geometry.rectangle.Rect
import org.mistutils.logic.ifFalse
import org.mistutils.math.fastFloor
import java.lang.IllegalStateException
import kotlin.math.log2

/**
 * Efficient implementation of Space2D that uses a hierarchical hashed grid.
 *
 * @param defaultGridSize the default / 'normal' size of the grid used for this space, in world units.  Defaults to 1.
 *        Use a value that is close to the typical size of the objects you want to store in the grid.
 * @param minFeatureSize a minimum reasonable size for objects you want to store.  You can store smaller objects, but they will be stored
 *        at the smallest available grid instead of an even smaller one, so if you have a lot of them in a small location then small queries in that
 *        area will have to loop all of them.
 *        Defaults to gridSize / 256
 * @param removeEmptyLayers a separate layer is created for objects for each power of two.  If you have objects that change size
 *        continuously (not by direct jumps to large/small sizes) by several magnitudes (e.g. 1m to 1000m), then many empty
 *        layers can be left if there are few other objects of those sizes.  The empty layers will be iterated in any queries,
 *        and slow down them marginally.  If this is set to true (default) then a layer is removed immediately when the last
 *        object is removed from it, avoiding this problem.
 * @param removeEmptyCells objects are stored in sets in hashtable grid cells.  If true, the set is removed and freed when the last
 *        object is removed from a grid cell, if false the set is left in the grid cell.  If you have few objects that move a lot
 *        over new places, it could save memory to remove empty grid cells (set this to true).
 *        If you have many objects that move in regular patterns, often revisiting the same spaces it could increase
 *        performance to keep empty grid cells (set this to false).  Defaults to true, to avoid memory 'leaking' in
 *        suboptimal cases.
 *
 */
// NOTE: Min feature sizes could be dynamic as well, increase or decrease when more than some amount of objects.
class HierarchicalSpace2D<T: Located2D>(
    val defaultGridSize: Double = 1.0,
    val minFeatureSize: Double = defaultGridSize / 256,
    val removeEmptyLayers: Boolean = true,
    val removeEmptyCells: Boolean = true): Space2D<T> {

    private val levels: IntObjMap<HierarchicalSpace2DLevel<T>> = HashIntObjMaps.newMutableMap<HierarchicalSpace2DLevel<T>>(16)

    init {
        // Check parameters
        Check.positive(defaultGridSize, "defaultGridSize")
        Check.positive(minFeatureSize, "minFeatureSize")
        Check.lessOrEqual(minFeatureSize, "minFeatureSize", defaultGridSize, "defaultGridSize")
    }

    val size: Int get() = levels.values.sumBy { it.size }

    override fun add(located: T) {
        // Check if object is a big object
        val space2DLevel = levels.computeIfAbsent(getLevelForSize(located.boundingSize)) { level: Int ->
            HierarchicalSpace2DLevel(defaultGridSize * Math.pow(2.0, level.toDouble()), removeEmptyCells)
        }
        space2DLevel.add(located)
    }

    override fun remove(located: Located2D): Boolean {
        // Check if object is a big object
        val levelId = getLevelForSize(located.boundingSize)
        val level = levels[levelId]
        val wasRemoved = level?.remove(located) ?: false

        if (wasRemoved && removeEmptyLayers && level != null && level.size <= 0) {
            // Level is now empty, remove it
            levels.remove(levelId)
        }

        return wasRemoved
    }

    override fun onMoved(located: Located2D, oldX: Double, oldY: Double) {
        levels[getLevelForSize(located.boundingSize)]?.onMoved(located, oldX, oldY) ?: throw IllegalStateException("The located object $located could not be found, can't move it.")
    }

    override fun onBoundingSizeChanged(located: Located2D, oldSize: Double) {
        // Check if level changed
        val oldLevel = getLevelForSize(oldSize)
        val newLevel = getLevelForSize(located.boundingSize)
        if (oldLevel != newLevel) {
            // Remove from old level
            remove(located).ifFalse { throw IllegalStateException("The located object $located could not be found, can't change size") }

            // Add to new level
            add(located as T)
        }
    }

    override fun visitObjects(
        area: Rect,
        minSize: Double,
        maxSize: Double,
        visitor: (T) -> Boolean
    ) {
        levels.forEach(IntObjConsumer { _, level ->
            // Check that the level size overlaps with the requested sizes
            if (level.maxObjectSize >= minSize &&
                level.minObjectSize <= maxSize) {
                level.visitObjects(area, minSize, maxSize, visitor)
            }
        })
    }

    override fun findNClosest(
        x: Double,
        y: Double,
        maxNum: Int,
        out: MutableList<LocationDistance<T>>,
        minDistance: Double,
        maxDistance: Double,
        minSize: Double,
        maxSize: Double,
        objectFilter: ((T) -> Boolean)?
    ): MutableList<LocationDistance<T>> {

        // NOTE: This could be optimized by searching outward separately on each level by the same amount, until
        // the required number of items is found.. (start by size of cell on lowest level, and expand diameter with *2 on each step until n found.)

        out.clear()

        val minDistanceSquared = minDistance * minDistance
        val maxDistanceSquared = maxDistance * maxDistance

        levels.forEach(IntObjConsumer { _, level ->
            // Check that the level size overlaps with the requested sizes
            if (level.maxObjectSize >= minSize &&
                level.minObjectSize <= maxSize) {
                level.findNClosest(x, y, maxNum, out, minDistanceSquared, maxDistanceSquared, minSize, maxSize, objectFilter)
            }
        })

        // Sort by distance
        out.sortBy { it.distanceSquared }

        // Drop extra
        if (out.size > maxNum) out.dropLast(out.size - maxNum)

        return out
    }

    private fun getLevelForSize(size: Double): Int {
        return log2(size.coerceAtLeast(minFeatureSize) / defaultGridSize).fastFloor()
    }


}
