package org.mistutils.collections.space

import java.lang.Math.sqrt

/**
 * A simple data class that holds a found located object and the (squared) distance from a search position to it.
 *
 * Returned by Space2D.findNClosest()
 */
data class LocationDistance<T>(
    var located: T,
    var distanceSquared: Double) {

    fun calculateDistance(): Double = sqrt(distanceSquared)
}