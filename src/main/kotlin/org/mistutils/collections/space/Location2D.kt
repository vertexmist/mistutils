package org.mistutils.collections.space

import org.mistutils.geometry.double2.Double2
import org.mistutils.geometry.int2.Int2

/**
 * Represents a location in some 2D Space with a bounding radius.
 */
open class Location2D(initialX: Double = 0.0,
                      initialY: Double = 0.0,
                      initialBoundingSize: Double = 0.0,
                      initialSpace: Space2D<out Located2D>? = null) : Located2D {

    private var posX: Double = initialX
    private var posY: Double = initialY

    override var x: Double
        get() = posX
        set(value) {setPos(value, posY)}

    override var y: Double
        get() = posY
        set(value) {setPos(posX, value)}

    override var boundingSize: Double = initialBoundingSize
        set(value) {
            val oldBoundingSize = field
            field = value
            notifySpaceWhenBoundingSizeChanged(oldBoundingSize)
        }

    override var space2D: Space2D<out Located2D>? = initialSpace
        set(value) {
            // Remove from old space
            field?.remove(this)

            field = value

            // Add to new space.  The new space must accept Location2D:s as members.
            (field as Space2D<Location2D>?)?.add(this)
        }

    /**
     * Sets the position of this location.  Also notifies the space it is contained in, updating its location there.
     */
    fun setPos(pos: Int2) = setPos(pos.x.toDouble(), pos.y.toDouble())

    /**
     * Sets the position of this location.  Also notifies the space it is contained in, updating its location there.
     */
    fun setPos(pos: Double2) = setPos(pos.x, pos.y)

    /**
     * Sets the position of this location.  Also notifies the space it is contained in, updating its location there.
     */
    fun setPos(x: Double, y: Double) {
        val oldX = posX
        val oldY = posY
        posX = x
        posY = y
        notifySpaceWhenMoved(oldX, oldY)
    }

}