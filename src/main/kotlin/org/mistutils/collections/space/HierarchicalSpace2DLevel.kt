package org.mistutils.collections.space

import com.koloboke.collect.map.LongObjMap
import com.koloboke.collect.map.hash.HashLongObjMaps
import com.koloboke.collect.set.hash.HashObjSet
import com.koloboke.collect.set.hash.HashObjSets
import org.mistutils.geometry.rectangle.Rect
import org.mistutils.logic.ifFalse
import org.mistutils.math.distanceSquared
import org.mistutils.math.fastFloor
import org.mistutils.math.squared
import org.mistutils.random.RandomHash
import java.lang.IllegalStateException
import kotlin.math.sqrt

/**
 * A level in a [HierarchicalSpace2D].
 *
 * Has an optimization for the common case of one object in each cell.
 * If many objects are added to a cell, a set is created and maintained for the cell.
 *
 * @param gridSize Size of the grid for this level in world units.
 * @param removeEmptyCells if true, when objects are removed from a cell then delete the set used in the cell as well.
 */
// TODO: A slight optimization could be to pool the sets instead of releasing and re-allocating them.  Also check if there are setmaps or bagmaps or such?
internal class HierarchicalSpace2DLevel<T: Located2D>(val gridSize: Double,
                                                      val removeEmptyCells: Boolean,
                                                      expectedObjectCount: Int = 10000) {

    private val hash: RandomHash = RandomHash.createDefault()

    // Optimization trick for holding only single values, no set object
    private val singleMap: LongObjMap<T> = HashLongObjMaps.newMutableMap(expectedObjectCount)

    // If more than one object is added to a cell, use this and store a set there.
    private val multiMap: LongObjMap<HashObjSet<T>> = HashLongObjMaps.newMutableMap(expectedObjectCount)

    val minObjectSize: Double get() = gridSize
    val maxObjectSize: Double get() = gridSize * 2.0

    /**
     * Number of objects in this layer
     */
    var size = 0
        private set

    fun add(located: T) {
        addToCell(getCellId(located), located)
    }

    fun remove(located: Located2D): Boolean {
        return removeFromCell(getCellId(located), located)
    }

    fun onMoved(located: Located2D, oldX: Double, oldY: Double) {
        val oldCellId = getCellId(oldX, oldY)
        val newCellId = getCellId(located)

        // If the locations map to same id there is no need to move
        if (oldCellId != newCellId) {
            // Different cells, move

            // Remove from old place, throw error if not found in expected location.
            removeFromCell(oldCellId, located).ifFalse { throw IllegalStateException("The located object $located was not found in the space at level $gridSize, position $oldX, $oldY") }

            // Add to new place, throws exception if located parameter is not of type T.
            addToCell(newCellId, located as T)
        }
    }

    private fun addToCell(cellId: Long, located: T) {
        // Try to add to single map first, if already occupied, use the multi-map
        if (singleMap.putIfAbsent(cellId, located) != null) {
            multiMap.computeIfAbsent(cellId, { HashObjSets.newMutableSet(4) }).add(located)
        }

        size++
    }

    private fun removeFromCell(cellId: Long, located: Located2D): Boolean {
        if (singleMap[cellId] == located) {
            // Was in single-map
            singleMap.remove(cellId)
            size--
            return true
        } else {
            // Look in multi-map
            val cellSet = multiMap.get(cellId)
            val wasRemoved = cellSet?.remove(located) ?: false
            if (wasRemoved) {
                size--

                if (removeEmptyCells && cellSet != null && cellSet.isEmpty) {
                    // Remove empty cell set
                    multiMap.remove(cellId)
                }
            }

            return wasRemoved
        }
    }

    fun visitObjects(area: Rect,
                     minSize: Double,
                     maxSize: Double,
                     visitor: (T) -> Boolean): Boolean {
        if (area.empty) return true

        // Keep these here so that we don't have to call intersect for the rect, as it is not inlined.
        val minX = area.minX
        val minY = area.minY
        val maxX = area.maxX
        val maxY = area.maxY

        val x1 = getGridCellX(minX)
        val y1 = getGridCellY(minY)
        val x2 = getGridCellX(maxX)
        val y2 = getGridCellY(maxY)

        // Iterate objects in all intersecting grids and their neighbours
        for (cellY in y1 - 1 .. y2 + 1)
            for (cellX in x1 - 1 .. x2 + 1) {
                if (!visitCell(getCellId(cellX, cellY), minSize, maxSize) {
                    val objX = it.x
                    val objY = it.y
                    val objRadius = it.boundingSize * 0.5
                    if (objX + objRadius >= minX &&
                        objX - objRadius <= maxX &&
                        objY + objRadius >= minY &&
                        objY - objRadius <= maxY) {
                        // Intersected object bounding box and is of requested size, notify visitor
                        visitor(it)
                    }
                    else true
                }) return false
            }
        return true
    }


    fun findNClosest(x: Double,
                     y: Double,
                     maxNum: Int,
                     out: MutableList<LocationDistance<T>>,
                     minDistanceSquared: Double,
                     maxDistanceSquared: Double,
                     minSize: Double,
                     maxSize: Double,
                     objectFilter: ((T) -> Boolean)?) {

        // Start at closest cell, work outwards from there until we have maxnum

        var foundObjects = 0
        var ring = 0
        var extraLoopDone = false

        while (!extraLoopDone && foundObjects < size && squared((ring - 1).coerceAtLeast(0) * gridSize) <= maxDistanceSquared) {

            if (foundObjects >= maxNum) extraLoopDone = true // Do an additional loop to make sure no better candidates are in an outer ring, stop after that

            visitInRing(x, y, ring, minDistanceSquared, maxDistanceSquared, minSize, maxSize, objectFilter) { obj, squaredDistance ->
                // Add result
                out.add(LocationDistance(obj, squaredDistance))

                // Found something
                foundObjects++

                true
            }

            ring++
        }
    }


    private inline fun visitInRing(x: Double,
                                   y: Double,
                                   radiusCells: Int,
                                   minDistanceSquared: Double,
                                   maxDistanceSquared: Double,
                                   minSize: Double,
                                   maxSize: Double,
                                   noinline filter: ((T) -> Boolean)?,
                                   visitor: (T, Double) -> Boolean): Boolean {

        // Check min radius against distance
        if (squared(SqrtOf2 * (radiusCells + 1) * gridSize) < minDistanceSquared) return true

        val centerCellX = getGridCellX(x)
        val centerCellY = getGridCellX(y)

        if (radiusCells <= 0) {
            // Only center case
            if (!visitCell(centerCellX, centerCellY, x, y, minSize, maxSize, minDistanceSquared, maxDistanceSquared, filter, visitor)) return false
        }
        else {
            // Visit cells along edges
            for (cellX in centerCellX - radiusCells .. centerCellX + radiusCells) {
                if (!visitCell(cellX, centerCellY - radiusCells, x, y, minSize, maxSize, minDistanceSquared, maxDistanceSquared, filter, visitor)) return false
                if (!visitCell(cellX, centerCellY + radiusCells, x, y, minSize, maxSize, minDistanceSquared, maxDistanceSquared, filter, visitor)) return false
            }
            for (cellY in centerCellY - radiusCells + 1 .. centerCellY + radiusCells - 1) {
                if (!visitCell(centerCellX - radiusCells, cellY, x, y, minSize, maxSize, minDistanceSquared, maxDistanceSquared, filter, visitor)) return false
                if (!visitCell(centerCellX + radiusCells, cellY, x, y, minSize, maxSize, minDistanceSquared, maxDistanceSquared, filter, visitor)) return false
            }
        }


        return true
    }


    private inline fun visitCell(
        cellX: Int,
        cellY: Int,
        x: Double,
        y: Double,
        minSize: Double,
        maxSize: Double,
        minDistanceSquared: Double,
        maxDistanceSquared: Double,
        noinline filter: ((T) -> Boolean)?,
        visitor: (T, Double) -> Boolean
    ): Boolean {
        // TODO: Optimize a bit by checking minimum and maximum distance to the cell?

        return visitCell(getCellId(cellX, cellY), minSize, maxSize, x, y, minDistanceSquared, maxDistanceSquared, filter, visitor)
    }

    private inline fun visitCell(
        cellId: Long,
        minSize: Double,
        maxSize: Double,
        x: Double,
        y: Double,
        minDistanceSquared: Double,
        maxDistanceSquared: Double,
        noinline filter: ((T) -> Boolean)?,
        visitor: (T, Double) -> Boolean
    ): Boolean {
        return visitCell(cellId, minSize, maxSize) {
            val squaredDistance = squaredDistance(x, y, it)
            val boundingRadiusSquared = it.boundingSize.squared * 0.5
            if (squaredDistance in minDistanceSquared - boundingRadiusSquared..maxDistanceSquared + boundingRadiusSquared &&
                filter?.invoke(it) != false) {

                visitor(it, squaredDistance)
            }
            else true
        }
    }

    private inline fun visitCell(
        cellId: Long,
        minSize: Double,
        maxSize: Double,
        visitor: (T) -> Boolean
    ): Boolean {
        // Check single map
        val singleItem = singleMap[cellId]
        if (singleItem != null) {
            val boundingSize = singleItem.boundingSize
            if (boundingSize in minSize..maxSize) {
                // Visit item as it was in correct size range
                if (!visitor(singleItem)) return false
            }
        }

        // Check multi map
        multiMap[cellId]?.forEach {
            val boundingSize = it.boundingSize
            if (boundingSize in minSize..maxSize) {
                // Visit item as it was in correct size range
                if (!visitor(it)) return@visitCell false
            }
        }

        return true
    }

    private inline fun squaredDistance(x: Double, y: Double, obj: T): Double {
        return distanceSquared(x, y, obj.x, obj.y)
    }

    private inline fun getCellId(located: Located2D): Long {
        return getCellId(located.x, located.y)
    }

    private inline fun getCellId(x: Double, y: Double): Long {
        val cellX = getGridCellX(x)
        val cellY = getGridCellY(y)
        return getCellId(cellX, cellY)
    }

    private inline fun getCellId(cellX: Int, cellY: Int): Long {
        return hash.hash(hash.hash(cellX) xor cellY.toLong())
    }

    private inline fun getGridCellX(value: Double): Int = (value / gridSize).fastFloor()
    private inline fun getGridCellY(value: Double): Int = (value / gridSize).fastFloor()

    companion object {
        val SqrtOf2 = sqrt(2.0)
    }


}