package org.mistutils.collections

import org.mistutils.random.RandomSequence

/**
 * Something that produces random entries of a specific type using a provided random sequence.
 */
interface RandomEntries<T> {

    /**
     * @returns a new random entry using the specified random sequence.
     */
    fun nextRandomEntry(randomSequence: RandomSequence): T
}