package org.mistutils.collections.map2d

import org.mistutils.geometry.double2.Double2
import org.mistutils.geometry.int2.Int2
import org.mistutils.geometry.int2.MutableInt2


/**
 * Maps a map position between world coordinates and map cell indexes.
 */
interface Map2DGeometry {
    fun getMapPosAtWorldPos(worldPos: Double2,
                            mapPosOut: MutableInt2 = MutableInt2()): Int2


}