package org.mistutils.collections.map2d

import org.mistutils.geometry.double2.Double2
import org.mistutils.geometry.double2.ImmutableDouble2
import org.mistutils.geometry.int2.Int2
import org.mistutils.geometry.int2.MutableInt2


/**
 * A regular grid geometry, consisting of [mapCellSize] sized grid cells, with cell 0,0 being located at [mapOffset]
 */
class GridGeometry(var mapCellSize: Double2 = ImmutableDouble2(1.0,1.0),
                   var mapOffset: Double2 = ImmutableDouble2(0.0, 0.0)): Map2DGeometry {

    override fun getMapPosAtWorldPos(worldPos: Double2,
                                     mapPosOut: MutableInt2
    ): Int2 {
        worldPos.floorDiv(mapCellSize, mapOffset, mapPosOut)
        return mapPosOut
    }
}