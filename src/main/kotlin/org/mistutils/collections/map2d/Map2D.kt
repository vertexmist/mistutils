package org.mistutils.collections.map2d

import org.mistutils.checking.Check
import org.mistutils.geometry.double2.Double2
import org.mistutils.geometry.int2.Int2
import java.util.*

/**
 * 2D map of something.
 */
// TODO: Add hex geometry
// TODO: Make geometries double way, returning screen world positions for a cell pos as well.
// TODO: Use map as backing? -> 'infinite' area.  Keep track of bounds, recaclulate if a grid is removed? (or only when requested)
// TODO: Multi-layer support?
// TODO: Support for positioned objects situated in grid, query for objects in given area?
open class Map2D<T>(initialSizeX: Int,
                    initialSizeY: Int,
                    val emptyCellFactory: (x: Int, y: Int, map: Map2D<T>) -> T,
                    val mapGeometry: Map2DGeometry = GridGeometry()) {

    protected var data = ArrayList<T>(initialSizeX*initialSizeY)

    var sizeX: Int = initialSizeX
        set(v) {
            Check.positive(v, "sizeX")
            if (field != v) {
                field = v
                resize(v, sizeY)
            }
        }

    var sizeY: Int = initialSizeY
        set(v) {
            Check.positive(v, "sizeY")
            if (field != v) {
                field = v
                resize(sizeX, v)
            }
        }

    final val dataSize: Int get() = sizeX * sizeY

    init {
        // Initialize
        for (y in 0 until sizeY) {
            for (x in 0 until sizeX) {
                data.add(emptyCellFactory(x, y, this))
            }
        }
    }

    operator fun get(x: Int, y: Int): T {
        return data[indexOf(x, y)]
    }

    operator fun get(pos: Int2): T {
        return get(pos.x, pos.y)
    }

    fun getOrDefault(x: Int, y: Int, default: T): T {
        return if (isInside(x, y)) get(x, y) else default
    }

    fun getOrNull(x: Int, y: Int): T? {
        return if (isInside(x, y)) get(x, y) else null
    }

    fun getOrNull(pos: Int2): T? {
        return if (isInside(pos)) get(pos) else null
    }

    fun getOrCalculate(x: Int, y: Int, calculator: (x: Int, y: Int)->T): T {
        return if (isInside(x, y)) get(x, y) else calculator(x, y)
    }

    operator fun set(x: Int, y: Int, value: T) {
        data[indexOf(x, y)] = value
    }

    operator fun set(pos: Int2, value: T) {
        set(pos.x, pos.y, value)
    }


    fun getAtPos(worldPos: Double2): T {
        return get(mapGeometry.getMapPosAtWorldPos(worldPos))
    }

    fun getAtPosOrNull(worldPos: Double2): T? {
        val mapPos = mapGeometry.getMapPosAtWorldPos(worldPos)
        return getOrNull(mapPos.x, mapPos.y)
    }

    fun setAtPos(worldPos: Double2, cell: T) {
        return set(mapGeometry.getMapPosAtWorldPos(worldPos), cell)
    }


    fun resize(newSizeX: Int, newSizeY: Int,
               sourceX: Int = 0, sourceY: Int = 0,
               fillFunction: (newX: Int, newY: Int) -> T = {x,y -> emptyCellFactory(x, y, this)}) {
        Check.positive(newSizeX, "newSizeX")
        Check.positive(newSizeY, "newSizeY")

        val newData = ArrayList<T>(newSizeX * newSizeY)

        for (y in 0 until sizeY) {
            for (x in 0 until sizeX) {
                val sx = sourceX + x
                val sy = sourceY + y
                newData.add(if (isInside(sx, sy)) get(sx, sy) else fillFunction(x, y))
            }
        }

        data = newData
        sizeX = newSizeX
        sizeY = newSizeY
    }

    fun isInside(x: Int, y: Int): Boolean {
        return x >= 0 && y >= 0 &&
               x < sizeX && y < sizeY
    }

    fun isInside(pos: Int2): Boolean = isInside(pos.x, pos.y)

    /**
     * Calculates a value for each cell in this Map2D, using a factory function.
     * @param cellFactory function that calculates the value to set to each cell in the map.
     */
    fun clear(cellFactory: () -> T) {
        for (y in 0 until sizeY) {
            for (x in 0 until sizeX) {
                val index = indexOf(x, y)
                data[index] = cellFactory()
            }
        }
    }

    /**
     * Calculates a value for each cell in this Map2D, based on the position.
     * @param fillFunction function that calculates the value to set to each cell in the map.
     * Gets the cell x and y coordinate as parameters.
     */
    fun fill(operation: (x: Int, y: Int, map: Map2D<T>) -> T) {
        for (y in 0 until sizeY) {
            for (x in 0 until sizeX) {
                val index = indexOf(x, y)
                data[index] = operation(x, y, this)
            }
        }
    }

    /**
     * Calculates a value for each cell in this Map2D, based on the position.
     * @param fillFunction function that calculates the value to set to each cell in the map.
     * Gets the cell x and y coordinate as parameters.
     */
    fun fill(operation: (x: Int, y: Int) -> T) {
        for (y in 0 until sizeY) {
            for (x in 0 until sizeX) {
                val index = indexOf(x, y)
                data[index] = operation(x, y)
            }
        }
    }

    /**
     * Calculates a value for each cell in this Map2D, based on the position and previous value.
     * @param operation function that calculates the value to set to each cell in the map.
     * Gets the cell x and y coordinate and previous cell value as parameters.
     */
    fun calculateWithCoordinates(operation: (x: Int, y: Int, previousValue: T) -> T) {
        for (y in 0 until sizeY) {
            for (x in 0 until sizeX) {
                val index = indexOf(x, y)
                data[index] = operation(x, y, data[index])
            }
        }
    }

    /**
     * Calculates a value for each cell in this Map2D, based on the previous value.
     * @param operation function that calculates the value to set to each cell in the map.
     * Gets the previous cell value as parameter.
     */
    fun calculate(operation: (previousValue: T) -> T) {
        for (y in 0 until sizeY) {
            for (x in 0 until sizeX) {
                val index = indexOf(x, y)
                data[index] = operation(data[index])
            }
        }
    }

    protected fun indexOf(x: Int, y: Int): Int {
        Check.inRange(x, "x", 0, sizeX)
        Check.inRange(y, "y", 0, sizeY)
        return y * sizeX + x
    }


}