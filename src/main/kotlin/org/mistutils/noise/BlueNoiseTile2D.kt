package org.mistutils.noise

import org.mistutils.checking.Check
import org.mistutils.collections.space.HierarchicalSpace2D
import org.mistutils.collections.space.Location2D
import org.mistutils.collections.space.Space2D
import org.mistutils.geometry.rectangle.Rect
import org.mistutils.random.RandomSequence
import org.mistutils.random.XoroShiro
import kotlin.math.min
import kotlin.math.roundToInt


class BlueNoiseTile2D (
    val maxPointCount: Int = 10000,
    val seed: Long = 0,
    val quality: Double = 10.0,
    val wrapTile: PlacementNoiseTile? = null): PlacementNoiseTile {

    /**
     * Points in the noise tile.  Distributed over tileArea with maximal distances.
     */
    val points: Space2D<Location2D> = HierarchicalSpace2D()
    val pointList =  ArrayList<Location2D>()

    init {
        buildPoints()
    }

    private fun buildPoints() {
        Check.greaterOrEqual(maxPointCount, "maxPointCount", 1)
        Check.greaterOrEqual(quality, "quality", 0.0)

        val wrapReference = wrapTile ?: this

        val random: RandomSequence = XoroShiro(seed)

        // Generate first point randomly, but avoid the edges
        val x = random.nextDouble(0.25, 0.75)
        val y = random.nextDouble(0.25, 0.75)
        val firstPoint = Location2D(x, y)
        points.add(firstPoint)
        pointList.add(firstPoint)

        val maxDistance = 3.0 // Sufficiently large to not be picked

        // Loop until we have enough points
        for (point in 2..maxPointCount) {

            // Try out candidates
            val candidateCount = (point * quality).roundToInt() + 1
            var bestCandidateX = 0.0
            var bestCandidateY = 0.0
            var bestCandidateSquareDistance = 0.0

            for (i in 1 .. candidateCount) {

                // Generate random candidate point
                val candidateX = random.nextDouble()
                val candidateY = random.nextDouble()

                // Evaluate distance to closest existing point
                var squaredDistanceToClosestPoint = points.distanceToClosestSquared(candidateX, candidateY) ?: maxDistance

                // Also consider all wrap-around neighbours
                for (yOffs in -1 .. 1)
                    for (xOffs in -1 .. 1) {
                        // Skip the cell itself, it was taken care of above
                        if (xOffs != 0 && yOffs != 0) {
                            val cx = candidateX + xOffs
                            val cy = candidateY + yOffs
                            squaredDistanceToClosestPoint =
                                min(squaredDistanceToClosestPoint, wrapReference.squaredDistanceToClosestPoint(cx, cy) ?: maxDistance)
                        }
                    }

                // Check if we found a better point than earlier candidates (further away from all other points)
                if (squaredDistanceToClosestPoint >= bestCandidateSquareDistance) {
                    bestCandidateSquareDistance = squaredDistanceToClosestPoint
                    bestCandidateX = candidateX
                    bestCandidateY = candidateY
                }
            }

            // Add best candidate
            val point = Location2D(bestCandidateX, bestCandidateY)
            points.add(point)
            pointList.add(point)
        }
    }

    override fun squaredDistanceToClosestPoint(x: Double, y: Double): Double? {
        return points.distanceToClosestSquared(x, y)
    }


    inline fun visitPoints(visitArea: Rect,
                           cellArea: Rect,
                           numberOfPointsToInclude: Int,
                           visitor: (index: Int, x: Double, y: Double) -> Boolean ): Boolean {

        val minX = cellArea.getRelativeX(visitArea.minX, true)
        val maxX = cellArea.getRelativeX(visitArea.maxX, true)
        val minY = cellArea.getRelativeY(visitArea.minY, true)
        val maxY = cellArea.getRelativeY(visitArea.maxY, true)

        for (i in 0 until numberOfPointsToInclude) {
            val point = pointList[i]
            if (point.x in minX .. maxX &&
                point.y in minY .. maxY) {

                if (!visitor(i, point.x, point.y)) return false
            }
        }

        return true
    }

}