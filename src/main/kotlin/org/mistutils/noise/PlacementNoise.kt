package org.mistutils.noise

import org.mistutils.field.Feature
import org.mistutils.field.FeatureField2D
import org.mistutils.field.Field2D
import org.mistutils.field.featurefields.FeatureField2DBase
import org.mistutils.field.featurefields.SimpleFeature
import org.mistutils.field.featurefields.typestrategies.ConstantTypeStrategy
import org.mistutils.field.featurefields.typestrategies.TypeStrategy
import org.mistutils.field.valuefields.ConstField2D
import org.mistutils.geometry.grid.RectangularGrid
import org.mistutils.geometry.rectangle.Rect
import org.mistutils.math.min
import org.mistutils.random.RandomSequence
import org.mistutils.random.XoroShiro
import org.mistutils.symbol.Symbol

/**
 * Produces blue-noise distributed points at a requested density.
 * In blue noise, points are distributed evenly (as far from other points as possible).
 *
 * The setup takes a while, as it generates a tiling set of points that it then uses later.
 */


// TODO: Very low or high densities could be supported by blending between two density size scales (so that density of max points on one == density of 1 point on other)
// However, that means that points on scales two magnitudes removed will disappear when increasing density.  For most applications this should not be a big problem.


class PlacementNoise(val seed: Long = 0,
                     val samplingGridSize: Double = 1.0,
                     val maxDensity: Double = 1000.0,
                     var densityField: Field2D,
                     var noiseStructureField: Field2D,
                     val tileCount: Int = 8,
                     var featureSizeField: Field2D = ConstField2D(1.0),
                     var typeStrategy: TypeStrategy = ConstantTypeStrategy(null),
                     var sampleSize: Double = samplingGridSize * 0.01): FeatureField2DBase() {

    private val samplingGrid = RectangularGrid(samplingGridSize)

    private val random: RandomSequence = XoroShiro(seed)
    private val tiles = ArrayList<BlueNoiseTile2D>()
    init {
        // Create blue noise tiles to use
        val primaryTile = BlueNoiseTile2D(seed = random.nextLong())
        tiles.add(primaryTile)
        for (i in 2 .. tileCount) {
            tiles.add(BlueNoiseTile2D(seed = random.nextLong(), wrapTile = primaryTile))
        }
    }

    override fun visitFeaturesIntersectingArea(
        area: Rect,
        minDetailSize: Double,
        visitedFeatureTypes: Set<Symbol>?,
        createUniqueFeatureInstances: Boolean,
        visitor: (feature: Feature) -> Boolean
    ): Boolean {
        val tempFeature = SimpleFeature()
        return samplingGrid.visitIntersecting(area) {cellPos, cellArea ->
            random.setSeed(seed, cellPos.x.toLong(), cellPos.y.toLong())
            val tile = tiles[random.nextInt(tileCount)]
            val centerX = cellArea.centerX
            val centerY = cellArea.centerY
            val density = densityField[centerX, centerY, samplingGridSize] min maxDensity
            val noiseStructure = noiseStructureField[centerX, centerY, samplingGridSize]
            val cellSeed = random.nextLong()

            visitPoints(tile, area, cellArea, density, noiseStructure) { pointSeed: Long, x: Double, y: Double ->
                random.setSeed(cellSeed, pointSeed)
                tempFeature.posX = x
                tempFeature.posY = y
                tempFeature.seed = random.nextLong()
                tempFeature.size = featureSizeField[x, y, sampleSize]
                tempFeature.type = typeStrategy.determineType(random, x, y, tempFeature.size, sampleSize)
                visitor(tempFeature)
            }
        }
    }

    private inline fun visitPoints(tile: BlueNoiseTile2D,
                                   visitArea: Rect,
                                   cellArea: Rect,
                                   densityPerUnitArea: Double,
                                   noiseStructure: Double,
                                   visitor: (seed: Long, x: Double, y: Double) -> Boolean ): Boolean {

        // Determine density magnitude used, and the two noise tiles to sample from



        TODO("Implement placement noise")
    }


}
