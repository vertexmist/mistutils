package org.mistutils.noise

interface PlacementNoiseTile {

    /**
     * Returns squared distance to the closest point, or null if there are no points in this tile.
     */
    fun squaredDistanceToClosestPoint(x: Double, y: Double): Double?

}