package org.mistutils.raster

import org.mistutils.checking.Check
import org.mistutils.math.fastFloor
import org.mistutils.math.mix


/**
 * Represents the data of a single two dimensional float raster.
 * The grid cells in the raster go from 0,0 (inclusive) to sizeX,sizeY (exclusive)
 * May be part of a MultiRaster, or used as-is.
 *
 * Creates a new Raster with the specified size and the specified backing array and the specified
 * spacing in the backing array (useful for interleaved backing arrays).
 * @param sizeX x size of the raster in grid cells.
 * @param sizeY y size of the raster in grid cells.
 * @param data read & write array with the values in the raster, laid out row by row, using the
 *              dataOffset, dataXSkip and dataYSkip values.    Defaults to a new float array of the required size to hold the data for this raster.
 * @param dataOffset offset to the start of the values for this raster in the data array.  Defaults to 0.
 * @param dataXStep total elements to step when moving from one value on a row to the next in the data array.  Must be at least one.    Defaults to 1.
 * @param dataYSkip extra elements to skip between each row in the data array.    Defaults to 0.
 */
class Raster(val sizeX: Int,
             val sizeY: Int = sizeX,
             val data: FloatArray = FloatArray(sizeX * sizeY),
             val dataOffset: Int = 0,
             val dataXStep: Int = 1,
             val dataYSkip: Int = 0) {

    /**
     * Step between beginning of each row in the data.  Used internally.
     */
    val dataRowStep: Int = sizeX * dataXStep + dataYSkip

    init {
        Check.positive(sizeX, "sizeX")
        Check.positive(sizeY, "sizeY")
        Check.positiveOrZero(dataOffset, "dataOffset")
        Check.notZero(dataXStep, "dataXStep")
        Check.notZero(dataYSkip, "dataYSkip")
        Check.inRange(getIndex(0, 0), "index of first pos", 0, data.size)
        Check.inRange(getIndex(sizeX-1, sizeY-1), "index of last pos", 0, data.size)
    }

    /**
     * Creates an independent deep copy of the specified source raster.
     * @param source raster to copy.
     */
    constructor(source: Raster): this(source.sizeX, source.sizeY) {
        copyFrom(source)
    }

    /**
     * @return true if the data array contains data other than the values associated with this raster,
     *         false if the data array only contains data for this raster, dataOffset is zero, dataXStep is one, and dataYSkip is zero.
     */
    val isDataInterleaved: Boolean get() =
        data.size != sizeX * sizeY || dataOffset != 0 || dataXStep != 1 || dataYSkip != 0


    /**
     * @return the value in the specified grid cell.  Throws exception if the coordinate is outside the raster.
     */
    inline fun get(x: Int, y: Int): Float {
        if (x < 0 || x >= sizeX ||
            y < 0 || y >= sizeY)
            throw IllegalArgumentException("The coordinate ($x,$y) is outside the raster (which has a size of $sizeX,$sizeY).")

        return data[getIndex(x, y)]
    }

    /**
     * Sets the specified value to the specified grid cell.  Throws exception if the coordinate is outside the raster.
     */
    inline fun set(x: Int, y: Int, value: Float) {
        if (x < 0 || x >= sizeX ||
            y < 0 || y >= sizeY)
            throw IllegalArgumentException("The coordinate ($x,$y) is outside the raster (which has a size of $sizeX,$sizeY).")

        data[getIndex(x, y)] = value
    }

    /**
     * @return the interpolated value at the specified coordinate.  Throws exception if the coordinate is outside the raster.
     * @param sampleSize sampling size to use, with sufficiently large sampling size several adjacent cells may be averaged.
     * Defaults to 0, that samples exactly one point at the highest resolution available.
     */
    inline fun sampleValue(x: Double, y: Double, sampleSize: Double = 0.0): Float {
        // NOTE: If we implement some kind of cached mipmapped versions of the raster,
        // we could support sampleSize as well.

        if (x < 0 || x > sizeX - 1 ||
            y < 0 || y > sizeY - 1)
            throw IllegalArgumentException("The coordinate ($x,$y) is outside the raster (which has a size of $sizeX,$sizeY).")

        val x0 = x.fastFloor()
        val y0 = y.fastFloor()
        val x1 = x0 + 1
        val y1 = y0 + 1
        val cx = (x - x0).toFloat()
        val cy = (y - y0).toFloat()
        val yr0 = mix(cx, data[getIndex(x0, y0)], data[getIndex(x1, y0)])
        val yr1 = mix(cx, data[getIndex(x0, y1)], data[getIndex(x1, y1)])
        return mix(cy, yr0, yr1)
    }

    /**
     * @return index of the specified coordinate in the data array.  Does not check boundaries.
     */
    inline fun getIndex(x: Int, y: Int): Int {
        return dataOffset + y * dataRowStep + x * dataXStep
    }


    /**
     * Replaces the contents of this raster with the other raster.
     * The rasters must have the same size, if not, an exception is thrown.
     * @param source raster to copy content from.
     */
    fun copyFrom(source: Raster) {
        checkSizeMatches(source)

        if (nonInterleavedOperationPossible(source)) {
            // Non-interleaved data, we can do a system array copy
            System.arraycopy(source.data, 0, data, 0, data.size)
        } else {
            // Copy interleaved data
            combine(source) { x, y, thisValue, otherValue ->
                otherValue
            }
        }
    }

    /**
     * Multiplies all the values of this raster with the specified scale.
     */
    fun multiply(scale: Float) {
        calculate { it * scale }
    }

    /**
     * Adds the specified value to all cells of this raster.
     */
    fun add(offset: Float) {
        calculate { it + offset }
    }

    /**
     * Multiplies all the values of this raster with the specified scale, and then adds the offset.
     */
    fun multiplyAdd(scale: Float, offset: Float = 0f) {
        calculate { it * scale + offset }
    }

    /**
     * Adds the other raster to this raster.
     * The rasters must be of the same size, or an exception is thrown.
     * @param other raster to add to this raster.
     * @param originalScale value to scale this raster with.
     * @param otherScale value to scale the other raster with.
     * @param offset value to add to the result at each cell.
     */
    fun add(other: Raster,
            originalScale: Float = 1f,
            otherScale: Float = 1f,
            offset: Float = 0f) {
        combine(other) { x, y, thisValue, otherValue ->
            (thisValue * originalScale) + (otherValue * otherScale) + offset
        }
    }

    /**
     * Multiplies this raster with the specified other raster.
     * Each cell value is calculated as:  cellValue = (cellValue * originalScale + originalOffset) + (sourceCellValue * otherScale + otherOffset) + offset.
     * The rasters must be of the same size, or an exception is thrown.
     * @param other raster to multiply with this raster.
     * @param originalScale value to scale this raster with before multiplying.
     * @param otherScale value to scale the other raster with before multiplying.
     * @param originalOffset value to add to this raster after scaling and before multiplying.
     * @param otherOffset value to add to the other raster after scaling and before multiplying.
     * @param offset value to add to the result after multiplying
     */
    fun multiply(other: Raster,
                 originalScale: Float = 1f,
                 otherScale: Float = 1f,
                 originalOffset: Float = 0f,
                 otherOffset: Float = 0f,
                 offset: Float = 0f) {
        combine(other) { x, y, thisValue, otherValue ->
            (thisValue * originalScale + originalOffset) * (otherValue* otherScale + otherOffset) + offset
        }
    }

    /**
     * Combines this raster with the other raster, calculating a new value for each cell in this raster based on the
     * value of the corresponding previous value in this raster and the other raster.
     * @param other other raster to combine with this.  The other raster is not modified.  Should be the same size as this raster.
     * @param operation operation to perform for each grid cell.  Gets as parameters the raster cell x and y position,
     * the previous value in this raster, and the corresponding value in the other raster,
     * and returns the new value for the cell in this raster as the result.
     */
    inline fun combine(other: Raster, operation: (x: Int, y: Int, thisValue: Float, otherValue: Float) -> Float) {
        checkSizeMatches(other)

        val otherData = other.data
        val otherXStep = other.dataXStep
        val otherYSkip = other.dataYSkip

        var thisIndex = dataOffset
        var otherIndex = other.dataOffset

        for (y in 0..sizeY - 1) {
            for (x in 0..sizeX - 1) {
                data[thisIndex] = operation(x, y, data[thisIndex], otherData[otherIndex])

                thisIndex += dataXStep
                otherIndex += otherXStep
            }
            thisIndex += dataYSkip
            otherIndex += otherYSkip
        }
    }

    /**
     * Calculates a value for each cell in this raster, based on the previous value in that cell.
     * @param operation operation to perform for each grid cell.
     * Gets the previous value as parameters.
     */
    inline fun calculate(operation: (previousValue: Float) -> Float) {
        if (!isDataInterleaved) {
            // Handle non-interleaved case separately, we can do a slightly faster loop
            val end = data.size - 1
            for (i in 0 .. end) {
                data[i] = operation(data[i])
            }
        }
        else {
            // Handle interleaved data
            var index = dataOffset
            for (y in 0..sizeY - 1) {
                for (x in 0..sizeX - 1) {
                    data[index] = operation(data[index])
                    index += dataXStep
                }
                index += dataYSkip
            }
        }
    }

    /**
     * Calculates a value for each cell in this raster, based on the previous value in that cell.
     * @param operation operation to perform for each grid cell.
     * Gets the raster cell x and y position and the previous value in this raster as parameters.
     */
    inline fun calculateWithCoordinates(operation: (x: Int, y: Int, previousValue: Float) -> Float) {
        var index = dataOffset
        for (y in 0..sizeY - 1) {
            for (x in 0..sizeX - 1) {
                data[index] = operation(x, y, data[index])
                index += dataXStep
            }
            index += dataYSkip
        }
    }

    /**
     * Calculates a value for each cell in this raster, based on the position.
     * @param operation operation to perform for each grid cell.  Gets the raster x and y coordinate as parameters.
     */
    inline fun fill(operation: (x: Int, y: Int) -> Float) {
        var index = dataOffset
        for (y in 0..sizeY - 1) {
            for (x in 0..sizeX - 1) {
                data[index] = operation(x, y)
                index += dataXStep
            }
            index += dataYSkip
        }
    }



    /**
     * Make sure this raster has the same size as the other raster, if not, throw an IllegalArgumentException.
     */
    fun checkSizeMatches(other: Raster) {
        Check.equal(other.sizeX, "other sizeX", sizeX, "target sizeX")
        Check.equal(other.sizeY, "other sizeY", sizeY, "target sizeY")
    }

    /**
     * @return true if this raster and the other raster are both non-interleaved.
     */
    fun nonInterleavedOperationPossible(other: Raster): Boolean {
        return !isDataInterleaved && !other.isDataInterleaved
    }
}
