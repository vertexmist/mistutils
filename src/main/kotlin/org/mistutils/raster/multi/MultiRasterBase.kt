package org.mistutils.raster.multi

import org.mistutils.checking.Check
import org.mistutils.raster.Raster
import org.mistutils.symbol.Symbol
import java.util.*

/**
 * Common functionality for multi raster implementations.
 */
abstract class MultiRasterBase(override val sizeX: Int,
                               override val sizeY: Int): MultiRaster {

    override val channels = ArrayList<Raster>()
    override val channelIds = ArrayList<Symbol>()

    /**
     * Keeps track of a new channel, or replaces a previous channel with the same id.
     * Not thread safe, should not be called at the same time as channels may be enumerated or changed from other threads.
     * @param channelId id of the channel.
     * @param channelRaster raster associated with the channel.
     */
    protected fun doAddChannel(channelId: Symbol, channelRaster: Raster) {
        Check.equal(channelRaster.sizeX, "x size of added channel", sizeX, "raster x size")
        Check.equal(channelRaster.sizeY, "y size of added channel", sizeY, "raster y size")

        if (hasChannel(channelId)) {
            // Replace
            val index = getChannelIndex(channelId)
            channels[index] = channelRaster
        }
        else {
            // Add
            channels.add(channelRaster)
            channelIds.add(channelId)
        }
    }

    /**
     * Removes an existing channel, if found.
     * Not thread safe, should not be called at the same time as channels may be enumerated or changed from other threads.
     * @param channelId id of the channel to remove.
     */
    protected fun doRemoveChannel(channelId: Symbol) {
        val channelIndex = getChannelIndex(channelId)
        channels.removeAt(channelIndex)
        channelIds.removeAt(channelIndex)
    }

}