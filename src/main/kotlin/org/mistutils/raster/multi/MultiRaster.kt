package org.mistutils.raster.multi

import org.mistutils.raster.Raster
import org.mistutils.symbol.Symbol
import java.util.*

/**
 * A two dimensional data raster with float values, composed of one or more channels.
 */
interface MultiRaster {

    /**
     * Width of the raster in grid cells.
     */
    val sizeX: Int

    /**
     * Height of the raster in grid cells.
     */
    val sizeY: Int

    /**
     * Number of channels.  All channel indexes are in the range 0 .. channelCount-1
     */
    val channelCount: Int get() = channels.size

    /**
     * The channels making up this multiraster.
     * Each channel is on an index corresponding to the channelIndex used for that channel.
     */
    val channels: List<Raster>

    /**
     * The identifiers of the channels available in this multiraster.
     * Each channel is on an index corresponding to the channelIndex used for that channel.
     */
    val channelIds: List<Symbol>

    /**
     * A map from channel ids to the channels available in this raster.
     */
    val channelsById: Map<Symbol, Raster> get() = channelIds.zip(channels).toMap()

    /**
     * A map from channel ids to the channel indexes used for those channels.
     */
    val channelIndexes: Map<Symbol, Int> get() = channelIds.withIndex().associate { it.value to it.index }

    /**
     * @return true if this raster has a channel with the specified id.
     */
    fun hasChannel(channelId: Symbol): Boolean = channelIds.contains(channelId)

    /**
     * The index used for the specified channel.
     * Throws an exception if the channel is unknown.
     */
    fun getChannelIndex(channelId: Symbol): Int = channelIds.indexOf(channelId).apply { if (this < 0) throw IllegalArgumentException("No channel '$channelId' found.")}

    /**
     * Channel id for the specified channel index.
     */
    fun getChannelId(channelIndex: Int): Symbol = channelIds.getOrElse(channelIndex, {throw IllegalArgumentException("No channel with index $channelIndex found")})

    /**
     * @param channelId the id of the channel to get.
     * @return the raster with the values for the specified channel.
     */
    fun getChannel(channelId: Symbol): Raster = getChannel(getChannelIndex(channelId))

    /**
     * @param channelIndex the index of the channel to get.
     * @return the raster with the values for the specified channel.
     */
    fun getChannel(channelIndex: Int): Raster = channels.getOrElse(channelIndex, {throw IllegalArgumentException("No channel with index $channelIndex found")})

    /**
     * Get the value of the specified channel at the specified raster grid location.
     * @param gridX grid x coordinate to get data for.  Starts at zero and goes to gridSizeX.
     * @param gridY grid y coordinate to get data for.  Starts at zero and goes to gridSizeY.
     * @param channelIndex channel to get data for
     * @return the data value at the specified location and channel.
     */
    operator fun get(gridX: Int, gridY: Int, channelIndex: Int): Float

    /**
     * Get the value of the specified channel at the specified raster grid location.
     * @param gridX grid x coordinate to get data for.  Starts at zero and goes to gridSizeX.
     * @param gridY grid y coordinate to get data for.  Starts at zero and goes to gridSizeY.
     * @param channelId channel to get data for
     * @return the data value at the specified location and channel.
     */
    operator fun get(gridX: Int, gridY: Int, channelId: Symbol): Float = get(gridX, gridY, getChannelIndex(channelId))

    /**
     * Set the raster grid location to the specified value
     * @param gridX grid x coordinate to get data for.  Starts at zero and goes to gridSizeX.
     * @param gridY grid y coordinate to get data for.  Starts at zero and goes to gridSizeY.
     * @param channelIndex channel to set data for
     * @param value value to set
     */
    operator fun set(gridX: Int, gridY: Int, channelIndex: Int, value: Float)

    /**
     * Set the raster grid location to the specified value
     * @param gridX grid x coordinate to get data for.  Starts at zero and goes to gridSizeX.
     * @param gridY grid y coordinate to get data for.  Starts at zero and goes to gridSizeY.
     * @param channelId channel to set data for
     * @param value value to set
     */
    operator fun set(gridX: Int, gridY: Int, channelId: Symbol, value: Float) = set(gridX, gridY, getChannelIndex(channelId), value)

    /**
     * Retrieve the values of all the channels at the specified grid location.
     * @param gridX grid x coordinate to get data for.
     * @param gridY grid y coordinate to get data for.
     * @param channelValuesOut array to write channel values to, indexed by channel id.
     * @return the dataOut parameter, for convenience.
     */
    fun getValues(gridX: Int, gridY: Int, channelValuesOut: FloatArray = FloatArray(channelCount)): FloatArray

    /**
     * Retrieve the values of all the channels at the specified grid location.
     * @param gridX grid x coordinate to get data for.
     * @param gridY grid y coordinate to get data for.
     * @param channelValuesOut map to write channel values to.
     * @return the dataOut parameter, for convenience.
     */
    fun getValuesAsMap(gridX: Int, gridY: Int, channelValuesOut: MutableMap<Symbol, Float> = HashMap()): MutableMap<Symbol, Float> {
        channelValuesOut.clear()
        for (id in channelIds) {
            channelValuesOut.put(id, get(gridX, gridY, id))
        }
        return channelValuesOut
    }

    /**
     * Set all channels of the specified grid location to the specified values
     * @param gridX grid x coordinate to set data for.
     * @param gridY grid y coordinate to set data for.
     * @param channelValues data for each channel, indexed by channel id.
     */
    fun setValues(gridX: Int, gridY: Int, channelValues: FloatArray)

    /**
     * Set the specified channels of the specified grid location to the specified values
     * @param gridX grid x coordinate to set data for.
     * @param gridY grid y coordinate to set data for.
     * @param channelValues channel id to channel value map.
     */
    fun setValuesAsMap(gridX: Int, gridY: Int, channelValues: Map<Symbol, Float>) {
        for (entry in channelValues) {
            set(gridX, gridY, entry.key, entry.value)
        }
    }

    /**
     * May calculate new values for cells in the multiraster, based on previous values and the x and y position in the raster.
     * @param operation operation to perform for each grid cell.
     * Gets the raster x and y coordinate, as well as a float array with the data values for each channel as parameters.
     */
    fun calculate(operation: (x: Int, y: Int, channelValues: FloatArray) -> Float)

    /**
     * Can be used to combine two different multirasters in some way.
     */
    fun combine(otherMultiraster: MultiRaster,
                operation: (x: Int, y: Int, thisChannelValues: FloatArray, otherChannelValues: FloatArray) -> Float)


}
