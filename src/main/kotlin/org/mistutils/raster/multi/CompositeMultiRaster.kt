package org.mistutils.raster.multi

import org.mistutils.raster.Raster
import org.mistutils.symbol.Symbol


/**
 * A MultiRaster implementation where channels can be added or removed on the fly.
 * The channel rasters are not interleaved by default (unless interleaved rasters are added).
 * Adding or removing channels is not thread safe.
 * @param sizeX x size of the raster.  Should not be zero.
 * @param sizeY y size of the raster.  Should not be zero.
 * @param initialChannels channels to add initially.
 */
class CompositeMultiRaster(sizeX: Int,
                           sizeY: Int = sizeX,
                           initialChannels: Map<Symbol, Raster> = emptyMap()) : MultiRasterBase(sizeX, sizeY) {

    /**
     * @param sizeX x size of the raster.  Should not be zero.
     * @param sizeY y size of the raster.  Should not be zero.
     * @param initialChannelIds ids of channels to create and add initially.
     */
    constructor(sizeX: Int,
                sizeY: Int = sizeX,
                initialChannelIds: List<Symbol>): this(sizeX, sizeY, initialChannelIds.associate { it to Raster(sizeX, sizeY) })

    init {
        for ((id, raster) in initialChannels) {
            addChannel(id, raster)
        }
    }

    override fun get(gridX: Int, gridY: Int, channelIndex: Int): Float {
        return channels[channelIndex].get(gridX, gridY)
    }

    override fun set(gridX: Int, gridY: Int, channelIndex: Int, value: Float) {
        channels[channelIndex].set(gridX, gridY, value)
    }

    override fun getValues(gridX: Int, gridY: Int, channelValuesOut: FloatArray): FloatArray {
        for(i in 0 .. channelValuesOut.size-1) {
            channelValuesOut[i] = channels[i].get(gridX, gridY)
        }
        return channelValuesOut
    }

    override fun setValues(gridX: Int, gridY: Int, channelValues: FloatArray) {
        for(i in 0 .. channelValues.size-1) {
            channels[i].set(gridX, gridY, channelValues[i])
        }
    }

    override fun calculate(operation: (x: Int, y: Int, channelValues: FloatArray) -> Float) {
        val chanCount = channelCount
        val values = FloatArray(chanCount)
        for (y in 0 .. sizeY-1) {
            for (x in 0 .. sizeX-1) {
                for(i in 0 .. chanCount-1) {
                    values[i] = channels[i].get(x, y)
                }

                operation(x, y, values)

                for(i in 0 .. chanCount-1) {
                    channels[i].set(x, y, values[i])
                }
            }
        }
    }

    override fun combine(other: MultiRaster,
                         operation: (x: Int, y: Int, thisChannelValues: FloatArray, otherChannelValues: FloatArray) -> Float) {
        val thisChanCount = channelCount
        val otherChanCount = other.channelCount
        val thisValues = FloatArray(thisChanCount)
        val otherValues = FloatArray(otherChanCount)
        for (y in 0 .. sizeY-1) {
            for (x in 0 .. sizeX-1) {
                // Get values
                for(i in 0 .. thisChanCount-1) {
                    thisValues[i] = channels[i].get(x, y)
                }

                for(i in 0 .. otherChanCount-1) {
                    otherValues[i] = other.channels[i].get(x, y)
                }

                // Do op
                operation(x, y, thisValues, otherValues)

                // Store values
                for(i in 0 .. thisChanCount-1) {
                    channels[i].set(x, y, thisValues[i])
                }
                for(i in 0 .. otherChanCount-1) {
                    other.channels[i].set(x, y, otherValues[i])
                }
            }
        }
    }

    /**
     * Adds a new channel with the specified id to this raster, if it does not already have such a channel.
     */
    fun addChannel(channelId: Symbol) {
        if (!hasChannel(channelId)) {
            doAddChannel(channelId, Raster(sizeX, sizeY))
        }
    }

    /**
     * Adds a new channel with the specified id and channel raster to this raster, replacing any existing channel with the same id.
     */
    fun addChannel(channelId: Symbol, raster: Raster) {
        doAddChannel(channelId, raster)
    }

    /**
     * Removes the specified channel from this raster, if the channel exists.
     */
    fun removeChannel(channelId: Symbol) {
        doRemoveChannel(channelId)
    }


}
