package org.mistutils.raster.multi

import org.mistutils.raster.Raster
import org.mistutils.symbol.Symbol
import java.util.*


/**
 * A MultiRaster that interleaves the data of its channels in a single large data array.
 * This speeds up access in tight loops if the channels at a specific location are usually accessed together.
 * Channels can not be added or removed after the InterleavedMultiRaster is created.
 * @param sizeX x size of the raster (must be positive)
 * @param sizeY y size of the raster (must be positive)
 * @param channels a collection with the ids of the channels to have in the raster,
 *                 in the order they should be interleaved at each pixel.
*/
class InterleavedMultiRaster(sizeX: Int, sizeY: Int, channels: Collection<Symbol>) : MultiRasterBase(sizeX, sizeY) {

    /**
     * @param sizeX x size of the raster (must be positive)
     * @param sizeY y size of the raster (must be positive)
     * @param channels the ids of the channels to have in the raster, in the order they should be interleaved at each pixel.
     */
    constructor(sizeX: Int, sizeY: Int, vararg channels: Symbol) : this(sizeX, sizeY, Arrays.asList<Symbol>(*channels))

    /**
     * interleaved data array containing the channel elements for each pixel in sequence.
     *         E.g. if we have three channels, R, G, and B in that order, the data array will contain pixel 0,0 of channel R,
     *         followed by pixel 0,0 of channel G, followed by pixel 0,0 of channel B, followed by pixel 1,0 of channel R,
     *         pixel 1,0 of channel G, etc.
     */
    val data: FloatArray = FloatArray(sizeX * sizeY * channels.size)

    override val channelCount: Int = channels.size


    init {
        // Create the channel rasters
        val xStep = channels.size
        var offset = 0
        for (channelId in channels) {
            val channelRaster = Raster(sizeX, sizeY, data, offset++, xStep, 0)
            doAddChannel(channelId, channelRaster)
        }
    }

    override fun get(gridX: Int, gridY: Int, channelIndex: Int): Float {
        return data[calculateDataIndex(gridX, gridY, channelIndex)]
    }

    override fun set(gridX: Int, gridY: Int, channelIndex: Int, value: Float) {
        data[calculateDataIndex(gridX, gridY, channelIndex)] = value
    }

    override fun getValues(gridX: Int, gridY: Int, channelValuesOut: FloatArray): FloatArray {
        checkCoordiantes(gridX, gridY)
        if (channelValuesOut.size != channelCount) throw IllegalArgumentException("channelValuesOut should be of size $channelCount, but was of size ${channelValuesOut.size}")

        System.arraycopy(data, calculateRawDataIndex(gridX, gridY, 0), channelValuesOut, 0, channelCount)
        return channelValuesOut
    }

    override fun setValues(gridX: Int, gridY: Int, channelValues: FloatArray) {
        checkCoordiantes(gridX, gridY)
        if (channelValues.size != channelCount) throw IllegalArgumentException("channelValues should be of size $channelCount, but was of size ${channelValues.size}")

        System.arraycopy(channelValues, 0, data, calculateRawDataIndex(gridX, gridY, 0), channelCount)
    }

    override fun calculate(operation: (x: Int, y: Int, channelValues: FloatArray) -> Float) {
        val values = FloatArray(channelCount)
        for (y in 0 .. sizeY-1) {
            for (x in 0 .. sizeX-1) {
                val dataIndex = calculateRawDataIndex(x, y, 0)
                System.arraycopy(data, dataIndex, values, 0, channelCount)

                operation(x, y, values)

                System.arraycopy(values, 0, data, dataIndex, channelCount)
            }
        }
    }

    override fun combine(other: MultiRaster,
                         operation: (x: Int, y: Int, thisChannelValues: FloatArray, otherChannelValues: FloatArray) -> Float) {
        val thisChanCount = channelCount
        val otherChanCount = other.channelCount
        val thisValues = FloatArray(thisChanCount)
        val otherValues = FloatArray(otherChanCount)
        for (y in 0 .. sizeY-1) {
            for (x in 0 .. sizeX-1) {
                val thisDataIndex = calculateRawDataIndex(x, y, 0)

                // Get values
                System.arraycopy(data, thisDataIndex, thisValues, 0, channelCount)

                for(i in 0 .. otherChanCount-1) {
                    otherValues[i] = other.channels[i].get(x, y)
                }

                // Do op
                operation(x, y, thisValues, otherValues)

                // Store values
                System.arraycopy(thisValues, 0, data, thisDataIndex, channelCount)

                for(i in 0 .. otherChanCount-1) {
                    other.channels[i].set(x, y, otherValues[i])
                }
            }
        }
    }

    inline fun calculateDataIndex(gridX: Int, gridY: Int, channelIndex: Int): Int {
        checkCoordiantes(gridX, gridY)
        checkChannelIndex(channelIndex)
        return calculateRawDataIndex(gridX, gridY, channelIndex)
    }

    inline fun calculateRawDataIndex(gridX: Int,
                                     gridY: Int,
                                     channelIndex: Int) = channelCount * (gridX + sizeX * gridY) + channelIndex

    inline fun checkChannelIndex(channelIndex: Int) {
        if (channelIndex < 0 || channelIndex >= channelCount) throw IllegalArgumentException("Channel index ($channelIndex) out of bounds ($channelCount).")
    }

    inline fun checkCoordiantes(gridX: Int, gridY: Int) {
        if (gridX < 0 || gridX >= sizeX ||
            gridY < 0 || gridY >= sizeY) throw IllegalArgumentException("Coordinates ($gridX, $gridY) out of bounds ($sizeX, $sizeY).")
    }


}
