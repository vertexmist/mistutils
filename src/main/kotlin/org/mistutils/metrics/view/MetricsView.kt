package org.mistutils.metrics.view

import javafx.application.Platform
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.geometry.Side
import javafx.scene.Parent
import org.mistutils.metrics.Metrics
import javafx.scene.Scene
import javafx.stage.Stage
import javafx.scene.chart.XYChart
import javafx.scene.chart.LineChart
import javafx.scene.chart.NumberAxis
import org.mistutils.math.min
import org.mistutils.metrics.Metric
import org.mistutils.metrics.MetricsListener
import org.mistutils.ui.runOnJavaFXThread
import java.lang.IllegalStateException
import kotlin.math.max


/**
 * Simple view for showing metrics over time.
 * Opens a new JavaFX window.
 * JavaFX needs to be running when showMetrics is called.
 */
// TODO: Inputs for dataPointsProperty, timeStepProperty and showRawSamplesProperty
// TODO: Switch various metric visualizations on or off
// TODO: Multiple value axis?
// TODO: Creates too many temporary objects, override a panel and draw the graph manually using a simple ad-hoc line graph lib.
class MetricsView(val metrics: Metrics,
                  dataPointsToShow: Int = 100,
                  timeStep: Double = 0.1,
                  showRawSamples: Boolean = true,
                  val width: Double = 800.0,
                  val height: Double = 600.0) {

    val dataPointsProperty = SimpleIntegerProperty(dataPointsToShow)
    val timeStepProperty = SimpleDoubleProperty(timeStep)
    val showRawSamplesProperty = SimpleBooleanProperty(showRawSamples)

    private var initialized = false
    private var buildRequested = false

    private lateinit var stage: Stage
    val seriesByName = HashMap<String, XYChart.Series<Number, Number>>()
    lateinit var chart: LineChart<Number, Number>
    lateinit var timeAxis: NumberAxis
    lateinit var valueAxis: NumberAxis

    init {
        metrics.addListener(object: MetricsListener {
            override fun metricAdded(metrics: Metrics, metric: Metric<*>) {
                if (initialized) {
                    // Only add metric if already initialized.  If not, all metrics will anyway be added on initialization
                    Platform.runLater {
                        buildSeries(metric)
                    }
                }
            }

            override fun sampleAdded(metrics: Metrics, metric: Metric<*>) {
                if (initialized) {
                    // Only update data if already initialized
                    Platform.runLater {
                        updateMetric(metric)
                    }
                }
            }
        })
    }

    /**
     * True if the metrics view window is open, false if not.
     * Can be written to to change the window visibility.
     */
    var visible: Boolean = false
        set(value) {
            if (field != value) {
                field = value
                if (value) show()
                else hide()
            }
        }

    /**
     * Show the metrics view.
     * Starts JavaFX if necessary to show the metrics view.
     */
    fun show() {
        runOnJavaFXThread {
            if (!initialized) {
                initialized = true
                stage = Stage()
                stage.scene = Scene(buildUi(), width, height)
                stage.title = metrics.title
            }
            stage.show()
        }
    }

    /**
     * Hide the metrics view, if it is visible.
     */
    fun hide() {
        if (initialized) {
            Platform.runLater {
                stage.hide()
            }
        }
    }

    private fun buildUi(): Parent {
        // Don't build twice
        if (!buildRequested) buildRequested = true
        else return chart

        timeAxis = NumberAxis()
        valueAxis = NumberAxis()

        timeAxis.label = "time"
        timeAxis.isForceZeroInRange = false
        timeAxis.isMinorTickVisible = true
        timeAxis.tickUnit = 1.0
        timeAxis.minorTickCount = 5
        valueAxis.label = "values"
        valueAxis.side = Side.RIGHT
        //timeAxis.isMinorTickVisible = false

        chart = LineChart(
            timeAxis, valueAxis
        )

        chart.animated = false
        chart.createSymbols = false

        chart.title = metrics.title

        for (metric in metrics.metrics.values) {
            buildSeries(metric)
        }

        return chart
    }

    private fun buildSeries(metric: Metric<*>) {

        // Don't re-create
        if (seriesByName.containsKey(metric.name)) return

        // Create series
        val series = XYChart.Series<Number, Number>()
        series.name = metric.name

        // Add to chart
        chart.data.add(series)

        // Store reference for later access
        seriesByName[metric.name] = series


        // Populate with empty points
        val pointCount = dataPointsProperty.get()
        for (i in 0 until pointCount) {
            series.data.add(XYChart.Data(0, 0.0))
        }

        // Listen to changes in number of points
        dataPointsProperty.addListener { observable, oldValue, newValue ->
            val target: Int = max(newValue.toInt(), 0)
            if (series.data.size != target) {
                // Shrink if necessary
                series.data.dropLastWhile {
                    series.data.size > target
                }

                // Add if necessary
                while (series.data.size < target) {
                    series.data.add(XYChart.Data(0, 0.0))
                }

                // Redraw all data for this metric
                updateMetric(metric)
            }
        }

        // Update data for the metric
        updateMetric(metric)
    }

    private fun updateMetric(metric: Metric<*>) {
        val series = seriesByName[metric.name] ?: throw IllegalStateException("Could not find data series for metric ${metric.name}, but it should already have been created.  Probable bug in MetricsView.")

        // Reuse existing datapoints, just put them in correct places.
        val pointCount = series.data.size
        var time = metrics.elapsedTimeSeconds
        val timeStep = timeStepProperty.get()
        for (i in 0 until pointCount) {

            // Determine index to use
            val sampleIndex = if (showRawSamplesProperty.get()) {
                i
            }
            else {
                metric.getSampleIndexAtTime(time) ?: metric.size - 1
            }
            time -= timeStep // Go back from latest

            // Update chart data point
            if (sampleIndex >= 0 && sampleIndex < metric.size) {
                val datum = series.data[i]
                datum.xValue = metric.getTime(sampleIndex)
                datum.yValue = metric.getNumericValue(sampleIndex)
            }
        }

        // Update time axis to show the value
        updateTimeAxisBounds()
    }


    private fun updateTimeAxisBounds() {
        // Find earliest and latest time among all shown metrics
        var earliest: Double? = null
        var latest: Double? = null
        for (metric in metrics.metrics.values) {
            val metricEarliest = metric.getTime((dataPointsProperty.get() min metric.size) - 1)
            if (earliest == null) earliest = metricEarliest
            else {
                if (metricEarliest != null && metricEarliest < earliest) earliest = metricEarliest
            }

            val metricLatest = metric.latestTime
            if (latest == null) latest = metricLatest
            else {
                if (metricLatest != null && metricLatest > latest) latest = metricLatest
            }
        }

        // Update time axis
        timeAxis.lowerBound = earliest ?: 0.0
        timeAxis.upperBound = latest ?: 0.0
        timeAxis.isAutoRanging = false
        timeAxis.isForceZeroInRange = false
    }
}