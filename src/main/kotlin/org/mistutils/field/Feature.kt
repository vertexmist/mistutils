package org.mistutils.field

import org.mistutils.symbol.Symbol

/**
 * Something that can be found in a [FeatureField2D].
 * Usually when passed to a visitor, this is backed by a re-used instance, that should not be used outside
 * the visitor.  You can use the createCopy function to create a unique copy of the feature.
 */
interface Feature {

    /**
     * Position of this feature.
     */
    val posX: Double

    /**
     * Position of this feature.
     */
    val posY: Double

    /**
     * Diameter of this feature, used when determining if a feature overlaps some area, and to approximate the
     * visibility / level of detail of this feature.
     */
    val size: Double

    /**
     * Describes the type of this feature, or may be unspecified.
     * Could e.g. be used for retrieving an entity archetype or generator and instantiating an entity for the feature.
     */
    val type: Symbol?

    /**
     * Random seed for this feature.  Each feature should have a more or less unique seed if possible.
     */
    val seed: Long

    /**
     * Creates a unique copy, useful inside visitors that get passed a re-used Feature object but
     * want to store or send some of the features passed to them elsewhere.
     */
    fun createCopy(): Feature

}