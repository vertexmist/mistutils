package org.mistutils.field

import org.mistutils.geometry.double2.Double2

/**
 * A field that returns a specific object for each point, instead of a value.
 */
interface ObjectField2D<T: Any>: (Double, Double) -> T {

    override fun invoke(x: Double, y: Double): T  = get(x, y)

    /**
     * @returns the field object at the specified location, with a sample size of zero (infinitely small details).
     */
    operator fun get(x: Double, y: Double): T = get(x, y, 0.0)

    /**
     * Get object in the field at the specified position, and with the specified sample-size.
     */
    operator fun get(pos: Double2, sampleSize: Double = 0.0): T = get(pos.x, pos.y, sampleSize)

    /**
     * Get object in the field at the specified position, and with the specified sample-size.
     */
    fun get(x: Double, y: Double, sampleSize: Double): T

}