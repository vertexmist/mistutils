package org.mistutils.field.objectfields

import org.mistutils.field.Field2D
import org.mistutils.field.ObjectField2D
import org.mistutils.math.DoubleFun
import org.mistutils.interpolation.gradient.Gradient

/**
 * @param valueField a double value field that is sampled at the requested position, to get a value to pass to [valueToObject]
 * @param valueToObject a function that returns an object for a given value in the valueField.
 *        You could for example use a [Gradient] here.
 */
class ValueToObjectField<T: Any>(
    var valueField: Field2D,
    var valueToObject: DoubleFun<T>
): ObjectField2D<T> {

    override fun get(x: Double, y: Double, sampleSize: Double): T {
        // Get value at the location
        val value = valueField.get(x, y, sampleSize)

        // Get object for value
        return valueToObject(value)
    }
}