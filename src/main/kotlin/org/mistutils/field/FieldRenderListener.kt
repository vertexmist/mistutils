package org.mistutils.field

/**
 * Listen to rendering of a field to a raster.
 */
interface FieldRenderListener {

    /**
     * @param progress progress of the render, in 0..1 range
     * @return true to continue rendering, false to stop.
     */
    fun onRenderProgress(field: Field2D, progress: Double): Boolean

    /**
     * Called when the rendering is completed (not canceled)
     */
    fun onRenderComplete(field: Field2D)

}