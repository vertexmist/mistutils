package org.mistutils.field

import org.mistutils.geometry.rectangle.ImmutableRect
import org.mistutils.geometry.rectangle.Rect
import org.mistutils.symbol.Symbol

/**
 * A distribution of entities over an infinite field.
 * Each entity has a 2D position, a possible detailSize at which it should be taken into account,
 * a seed that can be used to generate any objects for it.
 *
 * Optionally the field could already be producing something like entities with components.
 *
 * The EntityField is queried by area or distance from a point.
 * By area one could get all entities within an area that have a detailSize in some range.
 * (If full entities with components are generated, then presence of components could also be used in the filter)
 *
 * The entity field can also be queried by a position, returning any entities in a specified detailSize range
 * that have a detailSize larger than the distance (optionally times some scaling factor)
 *
 */
// NOTE: For the sake of efficiency, we might want to assume that features have a rectangular bounding area instead of spherical
// TODO: It could make sense to make the queries to this into reactive -style streams?
interface FeatureField2D {

    /**
     * @param centerX the x position to find any intersecting features for.
     * @param centerY the y position to find any intersecting features for.
     * @param minDetailSize minimum feature size that will be visited (inclusive).  Any smaller features will not be visited.
     * @param visitedFeatureTypes if null, all feature types will be visited.  If not null then only the specified
     * feature types will be visited and other feature types will be skipped
     * @param createUniqueFeatureInstances if true a new Feature instance will be created for each feature and passed to the visitor
     * function.  This is expensive and should be avoided, unless you want to collect all the features to a list or similar.
     * By default this is false, which means that the same feature instance will be re-used and passed to the function
     * for each encountered feature.
     * @param visitor function that gets called for each feature encountered.  Returns true to continue visiting, false to abort.
     * @returns true if all features visited successfully, false if the visitor aborted.
     */
    fun visitFeaturesIntersectingPoint(centerX: Double,
                                       centerY: Double,
                                       minDetailSize: Double = 0.0,
                                       visitedFeatureTypes: Set<Symbol>? = null,
                                       createUniqueFeatureInstances: Boolean = false,
                                       visitor: (feature: Feature) -> Boolean): Boolean {
        // By default forward to a zero-sized area visitor.
        return visitFeaturesIntersectingArea(
            ImmutableRect(centerX, centerY, 0.0, 0.0, false),
            minDetailSize,
            visitedFeatureTypes,
            createUniqueFeatureInstances,
            visitor)
    }

    /**
     * @param area the rectangular area to find any intersecting features for.
     * @param minDetailSize minimum feature size that will be visited (inclusive).  Any smaller features will not be visited.
     * @param visitedFeatureTypes if null, all feature types will be visited.  If not null then only the specified
     * feature types will be visited and other feature types will be skipped
     * @param createUniqueFeatureInstances if true a new Feature instance will be created for each feature and passed to the visitor
     * function.  This is expensive and should be avoided, unless you want to collect all the features to a list or similar.
     * By default this is false, which means that the same feature instance will be re-used and passed to the function
     * for each encountered feature.
     * @param visitor function that gets called for each feature encountered.  Returns true to continue visiting, false to abort.
     * @returns true if all features visited successfully, false if the visitor aborted.
     */
    fun visitFeaturesIntersectingArea(area: Rect,
                                      minDetailSize: Double = 0.0,
                                      visitedFeatureTypes: Set<Symbol>? = null,
                                      createUniqueFeatureInstances: Boolean = false,
                                      visitor: (feature: Feature) -> Boolean): Boolean


}