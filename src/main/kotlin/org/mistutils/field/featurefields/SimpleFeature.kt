package org.mistutils.field.featurefields

import org.mistutils.field.Feature
import org.mistutils.symbol.Symbol

/**
 * Simple mutable implementation of Feature.
 */
data class SimpleFeature(
    override var posX: Double = 0.0,
    override var posY: Double = 0.0,
    override var size: Double = 1.0,
    override var type: Symbol? = null,
    override var seed: Long = 0): Feature {

    override fun createCopy(): Feature {
        return this.copy()
    }
}