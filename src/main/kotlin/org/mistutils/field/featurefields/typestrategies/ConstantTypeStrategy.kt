package org.mistutils.field.featurefields.typestrategies

import org.mistutils.random.RandomSequence
import org.mistutils.symbol.Symbol

/**
 * Returns the specified featureType
 */
class ConstantTypeStrategy(var featureType: Symbol?): TypeStrategy {
    override fun determineType(
        random: RandomSequence,
        x: Double,
        y: Double,
        featureSize: Double,
        sampleSize: Double
    ): Symbol? {
        return featureType
    }
}