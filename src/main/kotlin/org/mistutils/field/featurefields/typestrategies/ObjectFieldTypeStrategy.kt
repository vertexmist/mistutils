package org.mistutils.field.featurefields.typestrategies

import org.mistutils.field.ObjectField2D
import org.mistutils.random.RandomSequence
import org.mistutils.symbol.Symbol

/**
 * Determines feature type based on an field that returns the feature type
 * for each position.
 */
class ObjectFieldTypeStrategy(var objectField: ObjectField2D<Symbol>): TypeStrategy {
    override fun determineType(
        random: RandomSequence,
        x: Double,
        y: Double,
        featureSize: Double,
        sampleSize: Double
    ): Symbol? {
        return objectField.get(x, y, sampleSize)
    }
}