package org.mistutils.field.featurefields.typestrategies

import org.mistutils.field.Field2D
import org.mistutils.random.RandomSequence
import org.mistutils.symbol.Symbol

/**
 * Combines two TypeStrategies, selecting between them randomly based on their weights,
 * or based on a selection field.
 */
class CompositeTypeStrategy(var strategyA: TypeStrategy,
                            var strategyB: TypeStrategy,
                            var t: Double = 0.5,
                            var selectionField: Field2D? = null,
                            var selectionFieldScaling: Double = 1.0): TypeStrategy {
    override fun determineType(
        random: RandomSequence,
        x: Double,
        y: Double,
        featureSize: Double,
        sampleSize: Double
    ): Symbol? {
        val selection = t + (selectionField?.get(x, y, sampleSize) ?: 0.0) * selectionFieldScaling
        return if (random.nextBoolean(selection))
            strategyB.determineType(random, x, y, featureSize, sampleSize)
        else
            strategyA.determineType(random, x, y, featureSize, sampleSize)
    }
}