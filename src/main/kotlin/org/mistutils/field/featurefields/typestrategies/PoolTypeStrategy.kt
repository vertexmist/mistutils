package org.mistutils.field.featurefields.typestrategies

import org.mistutils.collections.WeightedMap
import org.mistutils.random.RandomSequence
import org.mistutils.symbol.Symbol

/**
 * A set of feature types with specified weights are selected from randomly based on the weights.
 */
class PoolTypeStrategy(var featureTypes: WeightedMap<Symbol> = WeightedMap()): TypeStrategy {
    override fun determineType(
        random: RandomSequence,
        x: Double,
        y: Double,
        featureSize: Double,
        sampleSize: Double
    ): Symbol? {
        val types = featureTypes

        return if (types.isEmpty) null
        else types.randomEntry(random)
    }
}