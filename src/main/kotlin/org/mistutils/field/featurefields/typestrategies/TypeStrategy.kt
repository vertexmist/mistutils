package org.mistutils.field.featurefields.typestrategies

import org.mistutils.random.RandomSequence
import org.mistutils.symbol.Symbol

/**
 * Used to determine the type of a feature created in a FeatureField.
 */
interface TypeStrategy {

    fun determineType(random: RandomSequence, x: Double, y: Double, featureSize: Double, sampleSize: Double = featureSize): Symbol?

}
