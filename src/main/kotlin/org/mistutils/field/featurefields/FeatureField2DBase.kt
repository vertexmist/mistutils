package org.mistutils.field.featurefields

import org.mistutils.field.Feature
import org.mistutils.field.FeatureField2D
import org.mistutils.geometry.rectangle.Rect
import org.mistutils.symbol.Symbol

/**
 * Common functionality for feature fields.
 */
abstract class FeatureField2DBase: FeatureField2D {

    protected inline fun shouldVisit(feature: Feature,
                                     visitArea: Rect,
                                     minDetailSize: Double,
                                     visitedFeatureTypes: Set<Symbol>?): Boolean {

        // Check size
        if (feature.size < minDetailSize) return false

        // Check distance
        if (!visitArea.intersectsCircle(feature.posX, feature.posY, feature.size * 0.5)) return false

        // Check types
        if (visitedFeatureTypes != null && !visitedFeatureTypes.contains(feature.type)) return false

        return true
    }

}