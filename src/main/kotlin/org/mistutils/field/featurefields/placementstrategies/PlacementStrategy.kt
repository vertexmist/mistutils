package org.mistutils.field.featurefields.placementstrategies

import org.mistutils.geometry.double2.MutableDouble2
import org.mistutils.geometry.int2.Int2
import org.mistutils.geometry.rectangle.Rect
import org.mistutils.random.RandomSequence

/**
 * Used to place a specific number of features inside a square cell.
 */
// TODO: amount of randomness / centerness, when centering adjust to left or right depending on even or odd rows to
//  approximate an optionally stacked hex pattern when randomness is low (add option to approximate square or hex pattern)
// Perhaps have a parameter for unevenness too, add gaussian to number of features (up to some max).
// This could maybe discard zero sized features, and optionally also create more features if that makes sense?
interface PlacementStrategy {

    fun placeFeature(random: RandomSequence, cell: Int2, cellArea: Rect, featureInCell: Int, featureCountInCell: Int, posOut: MutableDouble2)

}