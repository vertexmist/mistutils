package org.mistutils.field.featurefields.placementstrategies

import org.mistutils.geometry.double2.MutableDouble2
import org.mistutils.geometry.int2.Int2
import org.mistutils.geometry.rectangle.Rect
import org.mistutils.random.RandomSequence

/**
 * Simply place features randomly in the cell
 */
class RandomPlacementStrategy() : PlacementStrategy {
    override fun placeFeature(random: RandomSequence, cell: Int2, cellArea: Rect, featureInCell: Int, featureCountInCell: Int, posOut: MutableDouble2) {
        posOut.x = random.nextDouble(cellArea.minX, cellArea.maxX)
        posOut.y = random.nextDouble(cellArea.minY, cellArea.maxY)
    }
}