package org.mistutils.field.featurefields

import org.mistutils.field.Feature
import org.mistutils.geometry.rectangle.Rect
import org.mistutils.symbol.Symbol

/**
 * A feature field that contains a specified list of features.
 */
class ConstantFeatureField(var features: List<Feature>): FeatureField2DBase() {

    constructor(vararg features: Feature): this(features.toList())

    override fun visitFeaturesIntersectingArea(
        area: Rect,
        minDetailSize: Double,
        visitedFeatureTypes: Set<Symbol>?,
        createUniqueFeatureInstances: Boolean,
        visitor: (feature: Feature) -> Boolean
    ): Boolean {
        // Loop features
        for (feature in features) {

            // Filter
            if (feature.size >= minDetailSize &&
                (visitedFeatureTypes == null || visitedFeatureTypes.contains(feature.type)) &&
                area.intersectsCircle(feature.posX, feature.posY, feature.size * 0.5)) {

                // We do not need to create a new instance of features even if createUniqueFeatureInstances
                // is true, as we already have unique feature instances.
                if (!visitor(feature)) return false
            }
        }

        return true
    }
}

