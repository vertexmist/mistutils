package org.mistutils.field.featurefields

import org.mistutils.field.Feature
import org.mistutils.field.Field2D
import org.mistutils.field.featurefields.placementstrategies.PlacementStrategy
import org.mistutils.field.featurefields.typestrategies.ConstantTypeStrategy
import org.mistutils.field.featurefields.typestrategies.TypeStrategy
import org.mistutils.field.valuefields.ConstField2D
import org.mistutils.geometry.double2.MutableDouble2
import org.mistutils.geometry.grid.RectangularGrid
import org.mistutils.geometry.rectangle.MutableRect
import org.mistutils.geometry.rectangle.Rect
import org.mistutils.math.average
import org.mistutils.math.clampTo
import org.mistutils.math.clampToRange
import org.mistutils.random.RandomSequence
import org.mistutils.symbol.Symbol


/**
 * A value Field2D is used as a density function for a FeatureField.
 * A field can also be used to adjust the feature size.
 *
 * @param densityField number of features per unit area (e.g. if 1.0 is a meter, this density field indicates
 * number of features per square meters).  This is independent of the cellSize.  If the density is negative, no features
 * are added.  If the density is a fraction, a proportionally random number of features will be added.
 * @param seed a unique seed for this density feature field.
 */
class DensityFeatureField2D(
    var densityField: Field2D,
    var featureSizeField: Field2D = ConstField2D(1.0),
    var typeStrategy: TypeStrategy = ConstantTypeStrategy(null),
    var grid: RectangularGrid = RectangularGrid(),
    var maxFeatureCountInCell: Int = 3,
    var maxFeatureSize: Double = 1.0,
    var placementStrategy: PlacementStrategy,
    var seed: Long = 0): FeatureField2DBase() {

    override fun visitFeaturesIntersectingArea(
        area: Rect,
        minDetailSize: Double,
        visitedFeatureTypes: Set<Symbol>?,
        createUniqueFeatureInstances: Boolean,
        visitor: (feature: Feature) -> Boolean
    ): Boolean {
        val pos = MutableDouble2()
        val random = RandomSequence.createDefault()
        val feature = SimpleFeature()
        val samplingSize = average(grid.cellSizeX, grid.cellSizeY)

        // Need to visit area that is maxFeatureSize/2 bigger in all directions
        val largerArea = MutableRect(area)
        largerArea.resize(maxFeatureSize, maxFeatureSize, true)

        // Visit all cells
        return grid.visitIntersecting(largerArea) { cell, cellArea ->
            // Seed random with cell location and the seed for this density field
            random.setSeed(seed, cell.x.toLong(), cell.y.toLong())

            // Determine density at center of cell
            val density = densityField.get(cellArea.centerX, cellArea.centerY, samplingSize)

            // Determine number of features
            val featureCount = random.roundRandomly(density).clampTo(0, maxFeatureCountInCell)

            // Generate features to the cell
            for (i in 1 .. featureCount) {
                // Randomize a seed
                feature.seed = random.nextLong()

                // Determine position
                placementStrategy.placeFeature(random, cell, cellArea, i, featureCount, pos)
                val x = pos.x
                val y = pos.y
                feature.posX = x
                feature.posY = y
                feature.size = featureSizeField.get(x, y, maxFeatureSize * 0.5).clampToRange(0.0, maxFeatureSize)
                feature.type = typeStrategy.determineType(random, x, y, feature.size)

                // Check
                if (shouldVisit(feature, area, minDetailSize, visitedFeatureTypes)) {
                    // Create unique feature if wanted
                    val visitedFeature = if (createUniqueFeatureInstances) feature.createCopy() else feature

                    // Notify visitor
                    if (!visitor(visitedFeature)) return false
                }
            }

            true
        }
    }
}