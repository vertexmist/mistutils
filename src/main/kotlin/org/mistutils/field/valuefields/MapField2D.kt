package org.mistutils.field.valuefields

import org.mistutils.field.Field2D
import org.mistutils.interpolation.Interpolator
import org.mistutils.interpolation.interpolators.LinearInterpolator
import org.mistutils.math.clamp0To1

/**
 * Maps 2d field values using a function from height (double) to height (double).
 * Interpolators are (double)->double functions, so they can be used here if desired.
 * Try functions such as sin, round, sqrt, x^2, abs, etc.
 * You can also try constants, or offsets, or scaling.  In combination with a strength map these can be used to create
 * localized plateaus and cracks.
 *
 * The strength of the mapping function can also be adjusted, by blending it stronger in some areas than others,
 * using a mappingStrength field.
 *
 * @param sourceField a source field to map the value of.
 * @param mapper a mapping function to apply to the value of the source field.
 * @param mapperStrength a field describing how strongly the mapper function should be applied at each position.
 *        By default 0 = use original value, 1 = apply mapper to the fullest, and in between mix between the two alternatives
 *        using the specified interpolator.  The strengthScaling, strengthOffset, and clampStrength fields are
 *        applied to the mapperStrength field before use though.
 * @param strengthScaling Scaling factor to apply to the mapperStrength field before use
 * @param strengthOffset Offset to add to the mapperStrength field after scaling and before use
 * @param clampStrength if true, after the strength field has been scaled and offseted, it is clamped to the 0..1 range
 * before being used to mix the mapping and the original value.
 * @param strengthInterpolator interpolator to apply to the strengthValue after it has been scaled, offseted and possibly clamped.
 */
class MapField2D(var sourceField: Field2D,
                 var mapper: (Double) -> Double,
                 var mapperStrength: Field2D = ConstField2D(1.0),
                 var strengthScaling: Double = 1.0,
                 var strengthOffset: Double = 0.0,
                 var clampStrength: Boolean = true,
                 var strengthInterpolator: Interpolator = LinearInterpolator()): Field2D {

    override fun get(x: Double, y: Double, sampleSize: Double): Double {
        // Get source value
        val sourceValue = sourceField.get(x, y, sampleSize)

        // Apply mapping
        val mappedValue = mapper(sourceValue)

        // Get mapped strength and scale and offset it
        var strengthValue = mapperStrength.get(x, y, sampleSize) * strengthScaling + strengthOffset

        // Clamp if necessary
        if (clampStrength) strengthValue = strengthValue.clamp0To1()

        // Apply interpolator, use it to mix the source and mapped value
        return strengthInterpolator.interpolate(strengthValue, sourceValue, mappedValue)
    }
}