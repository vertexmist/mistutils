package org.mistutils.field.valuefields

import org.mistutils.field.Field2D
import org.mistutils.interpolation.Interpolator
import org.mistutils.interpolation.interpolators.LinearInterpolator
import org.mistutils.math.clamp0To1

/**
 * Mixes two fields using a third to specify the strength of each.
 *
 * @param fieldA source field whose values to use when the mixingField is close to zero.
 * @param fieldB source field whose values to use when the mixingField is close to one.
 * @param mixingField a field to use to mix the fields a and b.
 *        By default 0 = fieldA, 1 = fieldB, and in between mix between the two alternatives
 *        using the specified interpolator.  The mixScaling, mixOffset and clamp are
 *        applied to the mixingField field before use though.
 * @param mixScaling Scaling factor to apply to the mixingField field before use
 * @param mixOffset Offset to add to the mixingField field after scaling and before use
 * @param clamp if true, after the mixingField has been scaled and offseted, it is clamped to the 0..1 range
 * before being used to mix field a and b.
 * @param interpolator interpolator to use to interpolate between field a and b, taking the scaled and offseted and possibly clamped value of mixingField as input.
 */
class MixField2D(var fieldA: Field2D,
                 var fieldB: Field2D,
                 var mixingField: Field2D = ConstField2D(1.0),
                 var mixScaling: Double = 1.0,
                 var mixOffset: Double = 0.0,
                 var clamp: Boolean = true,
                 var interpolator: Interpolator = LinearInterpolator()): Field2D {

    override fun get(x: Double, y: Double, sampleSize: Double): Double {
        // Get source values
        val a = fieldA.get(x, y, sampleSize)
        val b = fieldB.get(x, y, sampleSize)

        // Get mixing value and scale and offset it
        var mixValue = mixingField.get(x, y, sampleSize) * mixScaling + mixOffset

        // Clamp if necessary
        if (clamp) mixValue = mixValue.clamp0To1()

        // Apply interpolator, use it to mix the source and mapped value
        return interpolator.interpolate(mixValue, a, b)
    }

}