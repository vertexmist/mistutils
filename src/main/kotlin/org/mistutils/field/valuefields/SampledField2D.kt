package org.mistutils.field.valuefields

import org.mistutils.field.Field2D
import org.mistutils.field.valuefields.sampling.FieldSampler

/**
 * Samples the specified [source] field using the specified [FieldSampler].
 * If no [sampleSpread] is specified the sampleSize is used.
 */
class SampledField2D(var source: Field2D,
                     var sampler: FieldSampler,
                     var sampleSpread: Double? = null): Field2D {

    override fun get(x: Double, y: Double, sampleSize: Double): Double {
        return sampler.sample(source, x, y, sampleSize, sampleSpread ?: sampleSize)
    }
}