package org.mistutils.field.valuefields

import org.mistutils.field.Field2D

/**
 * Combines two fields using some operator.
 */
class CombineField2D(
    var fieldA: Field2D,
    var fieldB: Field2D,
    var operator: (Double, Double) -> Double): Field2D {

    override fun get(x: Double, y: Double, sampleSize: Double): Double {
        val a = fieldA.get(x, y, sampleSize)
        val b = fieldB.get(x, y, sampleSize)
        return operator(a, b)
    }
}