package org.mistutils.field.valuefields

import org.mistutils.geometry.double2.Double2
import org.mistutils.geometry.double2.MutableDouble2
import org.mistutils.math.max
import org.mistutils.noise.OpenSimplexNoise
import org.mistutils.random.RandomSequence

/**
 * Two dimensional noise field.
 * The default value range is -1 .. 1
 * amplitude can be used to scale it, and resultOffset is added to it.
 *
 */
class NoiseField2D(var posScale: Double2 = MutableDouble2(1.0, 1.0),
                   var posOffset: Double2 = MutableDouble2(),
                   var amplitude: Double = 1.0,
                   var resultOffset: Double = 0.0,
                   seed: Long = RandomSequence.default.nextLong()): DetailField2DBase() {

    private var noise = OpenSimplexNoise(seed)

    /**
     * Seed used for the noise in this field.
     * Note that changing the seed is a somewhat costly operation, and should not be done in tight loops.
     */
    var seed: Long = seed
        set(value) {
            if (field != value) {
                field = value

                // Creates a unique noise for the specified seed.
                noise = OpenSimplexNoise(field)
            }
        }

    override fun detailSize(x: Double, y: Double): Double {
        return posScale.x max posScale.y
    }

    override fun getAverageValue(x: Double, y: Double): Double {
        return resultOffset
    }

    override fun getExactValue(x: Double, y: Double): Double {
        return noise.noise(x * posScale.x + posOffset.x,
                           y * posScale.y + posOffset.y) * amplitude + resultOffset
    }

}