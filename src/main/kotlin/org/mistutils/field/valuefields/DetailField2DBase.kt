package org.mistutils.field.valuefields

import org.mistutils.field.Field2D
import org.mistutils.math.map


/**
 * Common functionality for Field2D:s that have a known detail size.
 */
abstract class DetailField2DBase : Field2D {

    override fun get(x: Double, y: Double): Double = getExactValue(x, y)

    /**
     * Get value at the field at the specified position, and with the specified sample-size.
     */
    override fun get(x: Double, y: Double, sampleSize: Double): Double {
        val detailSize = detailSize(x, y)
        if (sampleSize < detailSize) {
            // Details lost in high frequency field, use average noise value
            return getAverageValue(x, y)
        }
        else {
            val value = getExactValue(x, y)

            val smallMixThreshold = (1.0 - blurThresholdFactor) * sampleSize
            return if (smallMixThreshold < detailSize) {
                // Detail starts to be lost in noise, mix with average noise value
                map(detailSize, smallMixThreshold, sampleSize, value, getAverageValue(x, y), true, false)
            }
            else {
                // Crisp detail
                value
            }
        }
    }

    /**
     * Return the exact value at the specified position (as if sample size was 0)
     */
    abstract fun getExactValue(x: Double, y: Double): Double

    /**
     * Size of details in the field near the specified place.
     * If the sample size is smaller than the detail size the get functions that take sample size parameters
     * will use the average value instead, to avoid aliasing artifacts in noise fields and the like.
     * A detail size of 0 means infinitely small details, and no blurring will happen.
     */
    abstract fun detailSize(x: Double = 0.0, y: Double = 0.0): Double

    /**
     * Average value near the specified position, used if sample size is smaller than the detail size at the location.
     */
    abstract fun getAverageValue(x: Double = 0.0, y: Double = 0.0): Double

    companion object {
        /**
         * The fraction of sample size where the transition between average field value and crisp field value
         * should be, when the sample size approaches the detail size in the field.
         * 0 = sharp transition from crisp to average value.  1 = continuous blurring all the time.
         */
        var blurThresholdFactor = 0.25
    }

}