package org.mistutils.field.valuefields.sampling

import org.mistutils.field.Field2D

/**
 * Samples the gradient along the x direction around the specified point.
 */
object GradientXSampler: FieldSampler {

    override fun sample(source: Field2D, x: Double, y: Double, sampleSize: Double, sampleSpread: Double): Double {
        return source[x+sampleSpread, y, sampleSize] -
               source[x-sampleSpread, y, sampleSize]
    }
}