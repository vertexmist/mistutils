package org.mistutils.field.valuefields.sampling

import org.mistutils.field.Field2D

/**
 * Samples the average value of the specified point.
 * Uses five sampling points, the location itself and the four neighbours.
 */
object AverageSampler: FieldSampler {

    override fun sample(source: Field2D, x: Double, y: Double, sampleSize: Double, sampleSpread: Double): Double {
        return 0.2 * (source[x, y, sampleSize] +
                      source[x-sampleSpread, y, sampleSize] +
                      source[x+sampleSpread, y, sampleSize] +
                      source[x, y-sampleSpread, sampleSize] +
                      source[x, y+sampleSpread, sampleSize])
    }
}