package org.mistutils.field.valuefields.sampling

import org.mistutils.field.Field2D

/**
 * Samples the gradient along the y direction around the specified point.
 */
object GradientYSampler: FieldSampler {

    override fun sample(source: Field2D, x: Double, y: Double, sampleSize: Double, sampleSpread: Double): Double {
        return source[x, y+sampleSpread, sampleSize] -
               source[x, y-sampleSpread, sampleSize]
    }
}