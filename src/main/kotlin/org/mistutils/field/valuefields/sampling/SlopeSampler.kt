package org.mistutils.field.valuefields.sampling

import org.mistutils.field.Field2D

/**
 * Samples the slope at a location.
 * Returns the square of the slope amount at the specified sampleSpread.
 */
object SlopeSampler: FieldSampler {

    override fun sample(source: Field2D, x: Double, y: Double, sampleSize: Double, sampleSpread: Double): Double {
        val dx = source[x+sampleSpread, y, sampleSize] -
                 source[x-sampleSpread, y, sampleSize]
        val dy = source[x, y+sampleSpread, sampleSize] -
                 source[x, y-sampleSpread, sampleSize]
        return dx*dx + dy*dy
    }
}