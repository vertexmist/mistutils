package org.mistutils.field.valuefields.sampling

import org.mistutils.field.Field2D

/**
 *
 */
object PointSampler : FieldSampler {

    override fun sample(source: Field2D, x: Double, y: Double, sampleSize: Double, sampleSpread: Double): Double {
        return source[x, y, sampleSize]
    }

    override fun wrap(source: Field2D, sampleSpread: Double?): Field2D {
        return source
    }
}