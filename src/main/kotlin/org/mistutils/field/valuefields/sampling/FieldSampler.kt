package org.mistutils.field.valuefields.sampling

import org.mistutils.field.Field2D
import org.mistutils.field.valuefields.SampledField2D

/**
 * A sampling strategy to use in [SampledField2D]
 */
interface FieldSampler {

    fun sample(source: Field2D, x: Double, y: Double, sampleSize: Double = 1.0, sampleSpread: Double = sampleSize): Double

    /**
     * Wrap the specified source field in this sampler.
     * @param sampleSpread spread to use for the sampler, or if null, use the sampleSize passed in as the spread.
     */
    fun wrap(source: Field2D, sampleSpread: Double? = null): Field2D {
        return SampledField2D(source, this, sampleSpread)
    }
}