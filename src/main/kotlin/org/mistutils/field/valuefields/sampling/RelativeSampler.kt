package org.mistutils.field.valuefields.sampling

import org.mistutils.field.Field2D

/**
 * Samples the value of a location compared to the average height around that location
 */
object RelativeSampler : FieldSampler {

    override fun sample(source: Field2D, x: Double, y: Double, sampleSize: Double, sampleSpread: Double): Double {
        val avg = 0.25 * (source[x-sampleSpread, y, sampleSize] +
                          source[x+sampleSpread, y, sampleSize] +
                          source[x, y-sampleSpread, sampleSize] +
                          source[x, y+sampleSpread, sampleSize])
        return source[x, y, sampleSize] - avg
    }
}