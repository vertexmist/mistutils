package org.mistutils.field.valuefields

import org.mistutils.field.Field2D

/**
 * Constant value field.
 */
class ConstField2D(var value: Double = 0.0) : Field2D {

    override fun get(x: Double, y: Double, sampleSize: Double): Double {
        return value
    }
}