package org.mistutils.field

import org.mistutils.checking.Check
import org.mistutils.geometry.double2.Double2
import org.mistutils.geometry.rectangle.ImmutableRect
import org.mistutils.geometry.rectangle.Rect
import org.mistutils.math.max
import org.mistutils.math.min
import org.mistutils.math.relPos
import org.mistutils.raster.Raster

/**
 * A two dimensional field with a single floating point value for each position.
 * Can be rendered to a raster or float array.
 */
// TODO: Add domain warping field2d:s, that is, warping of x and y coordinates, using polar coordinates, etc.
// NOTE: Could be called ValueField2D (as opposed to FeatureField2D), but it is unnecessarily cumbersome.
interface Field2D: (Double, Double) -> Double {

    /**
     * Implement a (x: Double, y: Double) -> Double function as well.
     */
    override fun invoke(x: Double, y: Double): Double = get(x, y)

    /**
     * @returns the field value at the specified location, with a sample size of zero (infinitely small details).
     */
    operator fun get(x: Double, y: Double): Double = get(x, y, 0.0)

    /**
     * Get value at the field at the specified position, and with the specified sample-size.
     */
    operator fun get(pos: Double2, sampleSize: Double = 0.0): Double = get(pos.x, pos.y, sampleSize)

    /**
     * Get value at the field at the specified position, and with the specified sample-size.
     */
    operator fun get(x: Double, y: Double, sampleSize: Double): Double

    /**
     * Writes the specified area from this field to the specified raster
     * @param targetRaster MultiRaster to render to
     * @param sourceArea source area from the field to render.  Defaults to 0,0 to 1,1
     * @param renderListener listener that is notified of rendering progress, or null if no listener specified.
     * @param progressTickIntervalMillis number of milliseconds between each call to the renderListener.
     *                                   Defaults to 100 (at most 10 updates per second).
     *                                   The listener is only checked each row, so on very wide and low targets this may not call as often as otherwise.
     */
    fun renderToRaster(targetRaster: Raster,
                       sourceArea: Rect = ImmutableRect.ZERO_TO_ONE,
                       renderListener: FieldRenderListener? = null,
                       progressTickIntervalMillis: Long = 100) {

        renderToArray(targetRaster.data,
                      targetRaster.sizeX,
                      targetRaster.sizeY,
                      sourceArea.minX,
                      sourceArea.minY,
                      sourceArea.maxX,
                      sourceArea.maxY,
                      targetRaster.dataOffset,
                      targetRaster.dataXStep,
                      targetRaster.dataYSkip,
                      renderListener,
                      progressTickIntervalMillis = progressTickIntervalMillis)
    }

    /**
     * Renders a part of this field to the target array
     * @param target array to render to
     * @param targetSizeX size of the target area to render to
     * @param targetSizeY size of the target area to render to
     * @param sourceStartX x position to start sampling the field from
     * @param sourceStartY y position to start sampling the field from
     * @param sourceEndX x position to end sampling the field from
     * @param sourceEndY y position to end sampling the field from
     * @param targetOffset offset to the start of the area to render to in the array
     * @param targetXStep step to apply to get from one element to the next in a row (1 == no extra steps between each element)
     * @param targetYSkip extra elements to skip between each target row
     * @param renderListener listener that is notified of rendering progress, or null if no listener specified.
     * @param sourceSampleSize sample size to use when sampling the field.
     * @param progressTickIntervalMillis number of milliseconds between each call to the renderListener.
     *                                   Defaults to 100 (at most 10 updates per second).
     *                                   The listener is only checked each row, so on very wide and low targets this may not call as often as otherwise.
     */
    fun renderToArray(target: FloatArray,
                      targetSizeX: Int,
                      targetSizeY: Int,
                      sourceStartX: Double = 0.0,
                      sourceStartY: Double = 0.0,
                      sourceEndX: Double = 1.0,
                      sourceEndY: Double = 1.0,
                      targetOffset: Int = 0,
                      targetXStep: Int = 1,
                      targetYSkip: Int = 0,
                      renderListener: FieldRenderListener? = null,
                      sourceSampleSize: Double = ((sourceEndX - sourceStartX) / (targetSizeX max 1)) min
                                                 ((sourceEndY - sourceStartY) / (targetSizeY max 1)),
                      progressTickIntervalMillis: Long = 100) {

        // Check parameters
        Check.positive(targetSizeX, "targetSizeX")
        Check.positive(targetSizeY, "targetSizeY")
        Check.positiveOrZero(targetOffset, "targetOffset")
        Check.notZero(targetXStep, "targetXStep")
        Check.positiveOrZero(sourceSampleSize, "sourceSampleSize")
        Check.positive(progressTickIntervalMillis, "progressTickIntervalMillis")

        // Initialize progress reporting counter
        var nextTickTime = System.currentTimeMillis() + progressTickIntervalMillis

        // Determine step
        val sourceXStep = (sourceEndX - sourceStartX) / (targetSizeX-1.0)
        val sourceYStep = (sourceEndY - sourceStartY) / (targetSizeY-1.0)

        // Loop the target raster cells over the target area and sample the field to get a value for each.
        var index = targetOffset
        var sourceY = sourceStartY
        for (y in 0 until targetSizeY) {

            // Reset source x position for each row
            var sourceX = sourceStartX
            for (x in 0 until targetSizeX) {

                // Sample value from field and assign it to the correct place in the array
                target[index] = get(sourceX, sourceY, sourceSampleSize).toFloat()

                // Step to next source and target location along x axis
                index += targetXStep
                sourceX += sourceXStep
            }

            // Step to next source and target location along y axis
            index += targetYSkip
            sourceY += sourceYStep

            // Notify listener if needed
            if (renderListener != null && System.currentTimeMillis() >= nextTickTime) {
                nextTickTime = System.currentTimeMillis() + progressTickIntervalMillis
                val continueRendering = renderListener.onRenderProgress(this, relPos(y, 0, targetSizeY - 1))
                if (!continueRendering) return
            }
        }

        // If we have a listener, inform it that we are ready
        renderListener?.onRenderComplete(this)
    }


}