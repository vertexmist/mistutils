package org.mistutils.files

import java.io.File
import java.io.RandomAccessFile
import java.util.*

/**
 * File related utility functions.
 */
object FileUtils {

    /**
     * @return the file size of the specified gzipped file.
     * The file is not sanity checked in any way, it's up to the called to ensure it's a gzipped file.
     * NOTE: If the uncompressed size is larger than 4GB, this will return an arbitrary value.
     */
    fun getGzipUncompressedSize(gzippedFile: File): Long {
        val raf = RandomAccessFile(gzippedFile, "r")
        try {
            // The uncompressed size is stored in the last four bytes of the compressed file.
            raf.seek(raf.length() - 4)
            val b4 = raf.read()
            val b3 = raf.read()
            val b2 = raf.read()
            val b1 = raf.read()
            return (b1.toLong() shl 24) or
                   (b2.toLong() shl 16) or
                   (b3.toLong() shl 8) or
                    b4.toLong()
        }
        finally {
            raf.close()
        }
    }

}


/**
 * Visit files matching the file filter under this directory and subdirectories matching the directory filter.
 * @param visitor method called for each matching file.  Takes the file and the parent path (as a list of directories) as parameters.
 */
fun File.recursivelyIterateFiles(visitor: (file: File, parentPath: List<File>) -> Unit,
                                 fileFilter: (File) -> Boolean,
                                 directoryFilter: (File) -> Boolean = { true },
                                 parentPath: List<File> = emptyList(),
                                 includeThisDirectoryInPath: Boolean = false) {

    if (this.isFile && fileFilter(this)) visitor(this, parentPath)
    else if (this.isDirectory && directoryFilter(this)) {
        val path = ArrayList(parentPath)
        if (includeThisDirectoryInPath) path.add(this)
        for (file in this.listFiles()) {
            file.recursivelyIterateFiles(visitor, fileFilter, directoryFilter, path, true)
        }
    }
}


/**
 * Lists files matching the file filter under this directory and subdirectories matching the directory filter.
 * @return list with pairs of subdirectory paths and files.  For files directly under this path the subdirectory path will be empty.
 */
fun File.recursivelyListFiles(fileFilter: (File) -> Boolean,
                              directoryFilter: (File) -> Boolean = { true },
                              outputList: MutableList<Pair<List<File>, File>> = java.util.ArrayList(),
                              parentPath: List<File> = emptyList(),
                              includeThisDirectoryInPath: Boolean = false): List<Pair<List<File>, File>> {

    if (this.isFile && fileFilter(this)) outputList.add(Pair(parentPath, this))
    else if (this.isDirectory && directoryFilter(this)) {
        val path = ArrayList(parentPath)
        if (includeThisDirectoryInPath) path.add(this)
        for (file in this.listFiles()) {
            file.recursivelyListFiles(fileFilter, directoryFilter, outputList, path, true)
        }
    }

    return outputList
}

/**
 * Creates a map with file paths being converted to keys, and files being converted to values.
 * @param fileFilter takes the path of the file, plus the file itself, and returns a key to use in the map for the value generated for the file.
 */
fun <K, V> File.recursivelyMapFiles(fileFilter: (File) -> Boolean,
                                    directoryFilter: (File) -> Boolean = {true},
                                    keyConverter: (List<File>) -> K,
                                    valueConverter: (File) -> V): MutableMap<K, V> {

    val map = LinkedHashMap<K, V>()

    val files = this.recursivelyListFiles(fileFilter, directoryFilter)
    for ((path, file) in files) {
        val p = ArrayList(path)
        p.add(file)
        val key = keyConverter(p)
        val value = valueConverter(file)
        map.put(key, value)
    }

    return map
}
