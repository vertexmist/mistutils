package org.mistutils.ui

import javafx.application.Platform
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableBooleanValue
import javafx.beans.value.ObservableValue
import javafx.collections.*
import javafx.event.ActionEvent
import javafx.geometry.Insets
import javafx.scene.Group
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.input.KeyCombination
import javafx.scene.layout.Border
import javafx.scene.shape.Line
import javafx.scene.shape.Rectangle
import javafx.stage.Window
import org.controlsfx.control.action.Action
import org.controlsfx.control.action.ActionGroup
import org.controlsfx.control.action.ActionUtils
import org.mistutils.math.min
import java.lang.IllegalStateException
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.reflect.KClass

/**
 * Adds a custom listener that is just a function passed as a parameter, that gets the new value of the property.
 * @return the added listener if needed for later removal.
 */
fun <T> ObservableValue<T>.addChangeListener(changeHandler: (T) -> Unit): ChangeListener<T> {
    val listener: ChangeListener<T> = ChangeListener { observable, oldValue, newValue -> changeHandler(newValue) }
    this.addListener(listener)
    return listener
}

/**
 * Add a block of code that gets run when an element is added to a list.
 * @return the added listener if needed for later removal.
 */
fun <T> ObservableList<T>.addAdditionListener(additionHandler: (T) -> Unit): ListChangeListener<T> {
    val listener = object: ListChangeListenerAdapter<T> {
        override fun onAdded(element: T) {
            additionHandler(element)
        }
    }
    this.addListener(listener)
    return listener
}

/**
 * Add a block of code that gets run when an element is removed from a list.
 * @return the added listener if needed for later removal.
 */
fun <T> ObservableList<T>.addRemovalListener(removalHandler: (T) -> Unit): ListChangeListener<T> {
    val listener = object: ListChangeListenerAdapter<T> {
        override fun onRemoved(element: T) {
            removalHandler(element)
        }
    }
    this.addListener(listener)
    return listener
}


/**
 * Asks a confirmation for some operation.
 * Return true if Yes was pressed, false if No was pressed or the dialog was closed (e.g. with escape).
 * @param description the question to ask, with complete information needed by the user.
 * @param title short version of the question or action about to be taken.
 * @param headerText general description of the problem
 * @param owner window to open dialog under.
 * If null, no parent is used.
 */
fun areYouSure(title: String,
               description: String = title,
               headerText: String? = null,
               owner: Window? = null): Boolean {
    val sure = Alert(Alert.AlertType.CONFIRMATION, description, ButtonType.YES, ButtonType.NO)
    sure.title = title
    sure.headerText = headerText
    sure.initOwner(owner)
    val result = sure.showAndWait()
    return result.isPresent && result.get() == ButtonType.YES
}

/**
 * Asks a confirmation for some operation.
 * Return true if Yes was pressed, false if No was pressed or the dialog was closed (e.g. with escape).
 * @param description the question to ask, with complete information needed by the user.
 * @param title short version of the question or action about to be taken.
 * @param headerText general description of the problem
 * @param parent any node of the window that opens the dialog.  Used to find the root window to center over.
 * If null, no parent is used.
 */
fun areYouSure(title: String,
               description: String = title,
               headerText: String? = null,
               parent: Node? = null): Boolean {
    return areYouSure(title, description, headerText, parent?.scene?.window)
}

/**
 * Update this list when the other list changes, using the specified converter to convert items to this list,
 * and the specified filter to determine if the element should be added to this list.
 *
 * Other elements can also be manually added to this list, as long as they don't equal a converted element from the other list
 * they will not be removed when elements are removed from the other list.
 *
 * Does not maintain the same order for the elements in both lists, added elements are added to the end, and permutations are ignored.
 *
 * @param otherList other list to replicate to this list
 * @param converter function to apply to elements of the other list when they are added to this list.
 * @param filter function that returns true for an element in the other list if it should be added to this list.
 * @return the listener that was registered with the otherList.
 *         To unbind, call otherList.removeListener() passing in the returned value as parameter.
 */
fun <T, S> ObservableList<T>.bindWithConverter(otherList: ObservableList<S>,
                                               converter: (S) -> T,
                                               filter: (S) -> Boolean = { true }): ListChangeListener<S> {

    // Mapping from other elements to local elements, so that we can handle removal of converted local elements when other elements change
    val mapping = IdentityHashMap<S, T>()

    // Reference to self
    val list = this

    // Add existing elements
    fun addOtherElement(otherElement: S) {
        if (filter(otherElement)) {
            val localElement = converter(otherElement)
            mapping.put(otherElement, localElement)
            list.add(localElement)
        }
    }
    for (otherElement in otherList) {
        addOtherElement(otherElement)
    }


    // Listen to further changes
    val listener: ListChangeListenerAdapter<S> = object : ListChangeListenerAdapter<S> {
        override fun onAdded(otherElement: S) {
            addOtherElement(otherElement)
        }

        override fun onRemoved(otherElement: S) {
            if (mapping.containsKey(otherElement)) {
                val localElement = mapping.get(otherElement)
                mapping.remove(otherElement)
                list.remove(localElement)
            }
        }
    }
    otherList.addListener(listener)

    // Return listener to allow later unbinding
    return listener
}

/**
 * Shows all the elements in this UI component, adding and removing children when the elements list updates.
 *
 * Other children can also be manually added to this list, as long as they don't equal a converted element from the other list
 * they will not be removed when elements are removed from the other list.
 *
 * Does not maintain the same order for the elements in both lists, added elements are added to the end, and permutations are ignored.
 *
 * @param elements list of elements to follow and show in the UI.
 * @param uiFactory function that creates a UI representation of a specific element in the elements list.
 *                  By default just creates a label with the toString output of the element.
 * @param filter filer that determines if an element should be added to the current node (if true) or not.
 * @return the listener that was registered with the elements.
 *         To unbind, call elements.removeListener() passing in the returned value as parameter.
 */
fun <T> Group.bindChildren(elements: ObservableList<T>,
                           uiFactory: (T) -> Node = { Label(it.toString()) },
                           filter: (T) -> Boolean = { true }): ListChangeListener<T> {
    return this.children.bindWithConverter(elements, uiFactory, filter)
}


/**
 * Sets the start and end coordinates of the line.
 */
fun Line.set(x1: Double, y1: Double, x2: Double, y2: Double) {
    this.startX = x1
    this.endX = x2
    this.startY = y1
    this.endY = y2
}

/**
 * Sets the start coordinates of the line.
 */
fun Line.setStart(x: Double, y: Double) {
    this.startX = x
    this.startY = y
}

/**
 * Sets the end coordinates of the line.
 */
fun Line.setEnd(x: Double, y: Double) {
    this.endX = x
    this.endY = y
}


/**
 * Sets the rectangle location and size based on the coordinates for two corners
 */
fun Rectangle.setCorners(x1: Double, y1: Double, x2: Double, y2: Double) {
    this.x = x1 min x2
    this.y = y1 min y2
    this.width = Math.abs(x2 - x1)
    this.height = Math.abs(y2 - y1)
}

/**
 * Sets the rectangle position and size
 */
fun Rectangle.set(x: Double, y: Double, w: Double, h: Double) {
    this.x = x
    this.y = y
    this.width = w
    this.height = h
}

/**
 * Sets the rectangle position
 */
fun Rectangle.setPos(x: Double, y: Double) {
    this.x = x
    this.y = y
}

/**
 * Sets the rectangle size
 */
fun Rectangle.setSize(w: Double, h: Double) {
    this.width = w
    this.height = h
}

/**
 * Shorthand for creating a new instance of an observable list,
 * optionally with an addition and removal listener
 * @param listener listener to add to this list.
 * Uses ListChangeListenerAdapter for increased convenience.
 */
fun <T> observableList(listener: ListChangeListenerAdapter<T>? = null): ObservableList<T> {
    val list = FXCollections.observableArrayList<T>()

    // Add listener if provided
    if (listener != null) list.addListener(listener)

    return list
}

/**
 * Shorthand for creating a new action with the specified settings.
 * @param name user readable name of the action
 * @param onAction code to be called when the action is invoked
 * @param description longer description of this action, e.g. for tooltips. (optional)
 * @param graphic icon or other graphic to use with this action. (optional)
 * @param accelerator keyboard shortcut for this action. (optional)
 * @param showConfirmationMessage provides a confirmation message to ask the user before this action is executed, if it is non-null.
 * @param disabledBinding observable value for whether this action is enabled or not. (optional)
 * @param actionCollection list to add this action to when it is created. (optional)
 * @param actionGroup ActionGroup  to add this action to when it is created. (optional)
 */
fun action(name: String,
           onAction: (ActionEvent) -> Unit,
           description: String? = null,
           graphic: Node? = null,
           accelerator: KeyCombination? = null,
           showConfirmationMessage: ((ActionEvent) -> String?)? = null,
           disabledBinding: ObservableBooleanValue? = null,
           actionCollection: MutableList<Action>? = null,
           actionGroup: ActionGroup? = null): Action {

    // If specified, include the confirmation message generator and a confirmation dialog display in the action.
    val actionCode = if (showConfirmationMessage != null) {
        { e: ActionEvent ->
            val msg = showConfirmationMessage(e)
            if (msg == null || areYouSure("$name?",
                                          msg,
                                          parent = e.source as? Node)) {
                onAction(e)
            }
        }
    }
    else onAction

    val action = Action(name, actionCode)
    if (description != null) action.longText = description
    if (graphic != null) action.graphic = graphic
    if (accelerator != null) action.accelerator = accelerator
    if (disabledBinding != null) action.disabledProperty().bind(disabledBinding)
    if (actionCollection != null) actionCollection.add(action)
    if (actionGroup != null) actionGroup.actions.add(action)

    return action
}


/**
 * Return a property that is associated with a specific key in this map, and has a nullable value.  Changes to the value associated with the key is reflected in the
 * value of the property, and vice-versa.
 * @param key the key to associate the property with
 * @param defaultValue a value that the property will be set to when there is no value associated with the key in the map,
 *        or when the value in the map is null, or when the value in the map is not [type] or a subtype of [type].
 *        Defaults to null if not specified.
 * @param type the type of the property.  If the value in the map is not of this type, the default value is used for the property instead.
 * @return a property bound to the value of the specified key in this map.
 */
fun <K, V, T: Any>ObservableMap<K, V>.createNullableLookupProperty(key: K,
                                                                   type: KClass<T>,
                                                                   defaultValue: T? = null): SimpleObjectProperty<T?> {

    val prop = SimpleObjectProperty<T>()

    val updateOngoing = AtomicBoolean(false)

    this.addListener { change: MapChangeListener.Change<out K, out V> ->
        if (change.key == key && !updateOngoing.getAndSet(true)) {
            if (change.wasRemoved() && !change.wasAdded()) {
                // Key was removed, set property to default value
                prop.value = defaultValue
            } else if (change.wasAdded()) {
                // If the value is null or not assignable to the specified type, set property to the default value,
                // otherwise set property the new value of the key
                val valueAdded = change.valueAdded
                prop.value = if (valueAdded != null &&
                    type.java.isAssignableFrom((valueAdded as Any).javaClass)) valueAdded as T
                else defaultValue
            }
            updateOngoing.set(false)
        }
    }

    prop.addChangeListener {
        if (!updateOngoing.getAndSet(true)) {
            this.put(key, it as V)
            updateOngoing.set(false)
        }
    }

    return prop as SimpleObjectProperty<T?>
}


/**
 * Return a property that is associated with a specific key in this map and has a non-nullable value.  Changes to the value associated with the key is reflected in the
 * value of the property, and vice-versa.
 * @param key the key to associate the property with
 * @param defaultValue a value that the property will be set to when there is no value associated with the key in the map,
 *        or when the value in the map is null, or when the value in the map is not [type] or a subtype of [type].
 * @param type the type of the property.  If the value in the map is not of this type, the default value is used for the property instead.
 * @return a property bound to the value of the specified key in this map.
 */
fun <K, V, T: Any>ObservableMap<K, V>.createLookupProperty(key: K,
                                                           type: KClass<T>,
                                                           defaultValue: T): SimpleObjectProperty<T> {

    return createNullableLookupProperty(key, type, defaultValue) as SimpleObjectProperty<T>
}


/**
 * Creates a toolbar with the actions in this ActionGroup.
 */
fun ActionGroup.createToolbar(actionTextBehavior: ActionUtils.ActionTextBehavior = ActionUtils.ActionTextBehavior.SHOW,
                              hideToolbarWhenEmpty: Boolean = true): ToolBar {
    // Create toolbar
    val toolBar = ActionUtils.createToolBar(this.actions, actionTextBehavior)

    if (hideToolbarWhenEmpty) {
        // Hide toolbar if there are no actions to show
        toolBar.visibleProperty().value = this.actions.isNotEmpty()
        this.actions.addListener(ListChangeListener {
            toolBar.visibleProperty().value = this.actions.isNotEmpty()
        })
    }

    return toolBar
}

/**
 * Creates a button bar with the actions in this ActionGroup.
 */
fun ActionGroup.createButtonBar(hideButtonBarWhenEmpty: Boolean = true,
                                hideButtonWhenDisabled: Boolean = true): ButtonBar {
    // Create toolbar
    val buttonBar = ActionUtils.createButtonBar(this.actions)

    if (hideButtonBarWhenEmpty) {
        // Hide button bar if there are no actions to show
        buttonBar.visibleProperty().value = this.actions.isNotEmpty()
        this.actions.addListener(ListChangeListener {
            buttonBar.visibleProperty().value = this.actions.isNotEmpty()
        })
    }

    if (hideButtonWhenDisabled) {
        for (button in buttonBar.buttons) {
            button.visibleProperty().bind(button.disableProperty().not())
        }
        buttonBar.buttons.addAdditionListener {
            it.visibleProperty().bind(it.disableProperty().not())
        }
    }

    return buttonBar
}

/**
 * Creates a button for the specified action.
 */
fun Action.createButton(actionTextBehavior: ActionUtils.ActionTextBehavior = ActionUtils.ActionTextBehavior.SHOW): Button {
    return ActionUtils.createButton(this, actionTextBehavior)
}

/**
 * @return the window that the specified node is in.
 */
fun Node.getWindow(): Window {
    return scene.window
}

/**
 * @return a scroll pane containing the specified node.
 */
fun Node.wrapInScrollPane(): ScrollPane {
    val scrollPane = ScrollPane(this)
    scrollPane.isFitToWidth = true
    scrollPane.vbarPolicy = ScrollPane.ScrollBarPolicy.AS_NEEDED
    scrollPane.padding = Insets.EMPTY
    scrollPane.border = Border.EMPTY
    return scrollPane
}


/**
 * Makes sure JavaFX is started, then run the specified code.
 * This may return before the code has been run.
 */
fun runOnJavaFXThread(codeToRunOnJavaFXThread: () -> Unit) {

    // Make sure it is running
    ensureJavaFXRunning()

    // Run code on JavaFX thread.
    Platform.runLater(codeToRunOnJavaFXThread)
}
