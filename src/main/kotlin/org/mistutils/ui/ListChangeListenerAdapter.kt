package org.mistutils.ui

import javafx.collections.ListChangeListener
import javafx.collections.ObservableList
import java.util.*

/**
 * An adapter for ListChangeListener that reads the clunky change communication mechanism and calls
 * different methods when elements are added or removed from a list one by one.
 */
interface ListChangeListenerAdapter<T>: ListChangeListener<T> {

    override fun onChanged(change: ListChangeListener.Change<out T>?) {
        if (change != null) {
            while (change.next()) {
                // Handle removals
                for (removedElement in change.removed) {
                    onRemoved(removedElement)
                }

                // Handle additions
                for (addedElement in change.addedSubList) {
                    onAdded(addedElement)
                }

                // Handle re-orderings
                if (change.wasPermutated()) {
                    val permChanges = permutations.get() as MutableMap<ListChangeListenerAdapter<T>, PermutationChange<T>>
                    val permChange = permChanges.getOrPut(this, { PermutationChange(change as ListChangeListener.Change<T>) })
                    permChange.change = change as ListChangeListener.Change<T>
                    onPermutated(permChange)
                }
            }
        }
    }

    /**
     * Called when an element is added to the list
     * @param element the added element
     */
    fun onAdded(element: T) {}

    /**
     * Called when an element is removed from the list
     * @param element the removed element
     */
    fun onRemoved(element: T) {}

    /**
     * Called when elements were not added or removed, but the list elements changed order.
     * @param permutation contains the mapping from the old indexes to the new indexes as a result of the permutation.
     *                    Also provides a method for applying the same permutation to another list.
     *                    This permutation object is only valid during this method call,
     *                    after this method returns it will be re-used in subsequent onPermutated calls.
     */
    fun onPermutated(permutation: PermutationChange<T>) {}


    /**
     * Holds information about a re-ordering of a list.
     * Note that this class will be re-used for each update call, so it
     * should not be stored or referenced by the listener after the return of the listening method.
     */
    class PermutationChange<T>(var change: ListChangeListener.Change<T>? = null) {

        /**
         * The list where the change happened.
         */
        val list: ObservableList<T> = change!!.list

        /**
         * @return the index that the element at the specified index was moved to
         */
        fun getNewIndexFor(oldIndex: Int): Int {
            return if (oldIndex >= change!!.from &&
                       oldIndex < change!!.to)  change!!.getPermutation(oldIndex)
            else if (oldIndex >= 0 && oldIndex < list.size) oldIndex
            else throw IndexOutOfBoundsException("The index $oldIndex was out of range for the list $list with size ${list.size}")
        }

        /**
         * Applies this permutation change to the other list.
         * @param offset offset to apply to indexes in this list when converting them to indexes in the other list.
         * @throw IndexOutOfBoundsException if a change would go out of range on the other list.
         */
        fun <S>applyToOtherList(otherList: MutableList<S>, offset: Int = 0) {
            for (oldIndex in change!!.from .. change!!.to - 1) {
                // Calculate index change
                val newIndex = change!!.getPermutation(oldIndex)
                val otherFromIndex = oldIndex + offset
                val otherToIndex = newIndex + offset

                // Move in other list
                otherList.add(otherToIndex, otherList.removeAt(otherFromIndex))
            }
        }
    }

    companion object {
        // Work-around as interfaces can not store values
        private val permutations = object: ThreadLocal<MutableMap<ListChangeListenerAdapter<Any?>, PermutationChange<Any?>>>()  {
            override fun initialValue(): MutableMap<ListChangeListenerAdapter<Any?>, PermutationChange<Any?>> {
                return IdentityHashMap()
            }
        }
    }
}



