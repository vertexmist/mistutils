package org.mistutils.ui

import javafx.application.Application
import javafx.stage.Stage
import java.lang.IllegalStateException
import java.util.concurrent.CountDownLatch
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread

private val javaFxProbablyRunning = AtomicBoolean(false)

/**
 * True when it is strongly suspected or known that javafx is running.
 * Still doesn't account for JavaFX being closed, or being started by someone else.
 * So overall not a very reliable source of information, don't threat it as such.
 */
val isJavaFXSuspectedToBeRunning: Boolean get() = javaFxProbablyRunning.get()


/**
 * Makes sure JavaFX is started, so that Platform methods work, and windows can be opened.
 */
fun ensureJavaFXRunning() {
    // Try to start if we did not already start it from here
    if (!isJavaFXSuspectedToBeRunning) {
        JavaFXStarter().startJavaFX()
    }
}


private val javaFxStarted = CountDownLatch(1)

/**
 * Dummy application used to ensure that the javafx process is started.
 * Do not use this directly, instead use [ensureJavaFXRunning] and [isJavaFXSuspectedToBeRunning].
 */
class JavaFXStarter: Application() {


    /**
     * This is part of Application, do not call from client code
     */
    override fun start(primaryStage: Stage?) {
        javaFxProbablyRunning.set(true)
        javaFxStarted.countDown()
    }

    /**
     * Starts java fx if it is not already started.
     * Returns once it has been started.
     */
    fun startJavaFX() {
        //Platform.startup(Runnable { })  // This seems to only be available in JDK 9 (and maybe higher, until they removed jfx)

        try {
            thread {
                // This doesn't return (unless an application is already running, and it errors out), so run in own thread
                launch(JavaFXStarter::class.java)
            }
        } catch (e: IllegalStateException) {
            // This probably means that JavaFX was already started
            javaFxProbablyRunning.set(true)
            javaFxStarted.countDown()
        }

        // Wait until started
        javaFxStarted.await()
    }
}