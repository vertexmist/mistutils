package org.mistutils.ui

import javafx.collections.ObservableList
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.Label
import javafx.scene.control.TitledPane
import javafx.scene.control.ToolBar
import javafx.scene.layout.*
import org.controlsfx.control.action.Action
import org.controlsfx.control.action.ActionGroup
import tornadofx.add
import tornadofx.property

/**
 * A titled pane where items can be added to the header.
 */
open class HeaderPane(initialTitle: String? = null,
                      initialContent: Node? = null): TitledPane() {

    val header = BorderPane()
    val headerLeft = HBox()
    val headerRight = HBox()
    val headerCenter = HBox()
    val actionsToolbar = ToolBar()

    val titleLabel = Label("")

    /**
     * The title text to show.  Use this instead of the text property.
     */
    val titleProperty = titleLabel.textProperty()
    var title by property{titleLabel.textProperty()}

    init {
        header.left = headerLeft
        header.center = headerCenter
        header.right = headerRight
        headerLeft.alignment = Pos.CENTER_LEFT
        headerCenter.alignment = Pos.CENTER
        headerRight.alignment = Pos.CENTER_RIGHT

        stripBorders(actionsToolbar)
        stripBorders(header)
        stripBorders(headerLeft)
        stripBorders(headerCenter)
        stripBorders(headerRight)

        headerLeft.children += titleLabel

        titleLabel.padding = Insets(0.0, 10.0, 0.0, 0.0)

        headerCenter.add(Pane())
        headerRight.add(actionsToolbar)

        text = ""

        // TODO: Make the header use all available horizontal space, so that right side content gets aligned to the right

        graphic = header

        if (initialTitle != null) title = initialTitle
        if (initialContent != null) content = initialContent
    }

    /**
     * Add an action to show in the header
     */
    fun addActionToHeader(action: Action) {
        actionsToolbar.add( action.createButton())
    }

    /**
     * Add actions to show in the header
     * The actions are added and removed as the ActionGroup changes.
     */
    fun addActionsToHeader(actionGroup: ActionGroup) {
        addActionsToHeader(actionGroup.actions)
    }

    /**
     * Add actions to show in the header
     * The actions are added and removed as the observable list changes.
     */
    fun addActionsToHeader(actions: ObservableList<Action>) {
        actionsToolbar.items.bindWithConverter(actions, {it.createButton()})
    }

    private fun stripBorders(pane: Region) {
        pane.padding = Insets.EMPTY
        pane.border = Border.EMPTY
    }
}