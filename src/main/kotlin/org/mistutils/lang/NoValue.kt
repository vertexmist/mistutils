package org.mistutils.lang

/**
 * Used as a value / type in a Tweak to indicate that it has no (meaningful) value.
 */
object NoValue {

    override fun toString(): String {
        return ""
    }
}