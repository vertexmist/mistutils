package org.mistutils.lang

import javafx.beans.property.ObjectProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import org.mistutils.lang.serialization.JsonTweakSerializer
import org.mistutils.lang.serialization.TweakSerializer
import org.mistutils.lang.tweakfun.TweakFun
import org.mistutils.strings.CodeBuilder
import org.mistutils.strings.CodeBuilding
import org.mistutils.strings.toSymbol
import org.mistutils.symbol.Symbol
import tornadofx.getProperty
import tornadofx.move
import tornadofx.property
import java.io.*

/**
 * Something with a value of a specific type, an optional setup function to calculate the initial value, and an
 * update function to update the value over time or for different inputs.
 * Also may have a parent and children.
 */
class Tweak<T: Any>(
    type: Class<T>,
    name: Symbol,
    val initialValue: T,
    description: String? = null,
    updater: TweakFun<T>? = null): ContextNode<T>, CodeBuilding {

    /**
     * Type of the value in this tweak.
     */
    override val type: Class<T> = type

    /**
     * Name / id of this tweak.  It is recommended to make this unique among the siblings of this tweak.
     * Only upper and lower case letters A-Z and underscores are allowed, as well as numbers if they are not the first character of the name.
     * Must not be empty.
     */
    override var name: Symbol by property(name)

    /**
     * Optional description or documentation for this tweak / value.
     */
    override var description: String?  by property(description)

    /**
     * Current value.
     */
    override var value: T by property(initialValue)

    /**
     * Function / expression that updates the value, based on the current state.
     */
    var updater: TweakFun<T>? by property(updater)

    /**
     * Parent of this tweak, or null if none.
     */
    override var parent: Tweak<*>? = null
        private set

    // Modifiable children
    private val childrenEditable: ObservableList<Tweak<*>> = FXCollections.observableArrayList()

    /**
     * Child tweaks that this tweak has.  This list should not (and can not) be directly modified,
     * instead use the [add] and [remove] methods to add and remove children.
     * Changes may be observed by registering a change listener to the list.
     */
    override val children: ObservableList<Tweak<*>> = FXCollections.unmodifiableObservableList(childrenEditable)

    /**
     * Listeners that get called after each update of this tweak.
     */
    private var updateListeners: ArrayList<(Tweak<T>)->Unit>? = null

    /**
     * Property for observing changes to the name.
     */
    val nameProperty: ObjectProperty<Symbol> = getProperty(Tweak<T>::name)

    /**
     * Property for observing changes to the description.
     */
    val descriptionProperty: ObjectProperty<String?> = getProperty(Tweak<T>::description)

    /**
     * Property for observing changes to the value.
     */
    val valueProperty: ObjectProperty<T> = getProperty(Tweak<T>::value)

    /**
     * Property for observing changes of the updater function.
     */
    val updaterProperty: ObjectProperty<TweakFun<T>?> = getProperty(Tweak<T>::updater)

    /**
     * The root of the tree that this tweak is in, obtained by following parents until a tweak without a parent is encountered.
     */
    override val root: Tweak<*> get() = parent?.root ?: this

    /**
     * True if this tweak is a root tweak (=has no parent).
     */
    override val isRoot: Boolean get() = parent == null

    init {
        updaterProperty.addListener { observable, oldValue, newValue ->
            if (oldValue != newValue) {
                // NOTE: If the name and type of the parameters
                //       are the same, they would not necessary need to be updated changed..

                // Remove any old parameters
                removeAllChildren()

                // Add new parameters with default values
                if (newValue != null) {
                    for (parameter in newValue.parameters) {
                        add(Tweak(
                            parameter.type as Class<Any>,
                            parameter.name,
                            parameter.initialValue(),
                            parameter.description,
                            null))
                    }
                }
            }
        }
    }

    /**
     * @return true if this tweak has the specified tweak as any level of parent.
     */
    fun hasParentage(tweak: Tweak<*>): Boolean {
        val p = parent
        return when (p) {
            null -> false
            tweak -> true
            else -> p.hasParentage(tweak)
        }
    }

    /**
     * @return the child with the specified name, or null if none found.
     */
    operator fun get(name: String): Tweak<*>? = get(name.toSymbol())

    /**
     * @return the child with the specified name, or null if none found.
     */
    override operator fun get(name: Symbol): Tweak<*>? = children.find { it.name == name }

    /**
     * @return the tweak referenced by the specified reference, or null if none found.
     */
    override operator fun get(ref: Ref): Tweak<*>? {
        return if (ref.fromRoot && !isRoot) root[ref]
        else {
            when {
                ref.head == null -> this
                ref.tail == null -> get(ref.head)
                else -> {
                    get(ref.head)?.get(ref.tail)
                }
            }
        }
    }

    /**
     * Initialize this tweak, by first initializing all children, assigning the initial value to itself.
     */
    fun reset() {
        // Initialize children
        children.forEach { it.reset() }

        // Set initial value
        value = initialValue
    }

    /**
     * Update the value of this Tweak, by first updating its children and then using an update function if defined to update itself.
     */
    fun update() {
        // Update children
        children.forEach { it.update() }

        // Update self
        val f = updater
        if (f != null) {
            value = f.calculate(this)
        }

        // Notify update listeners if there are any
        updateListeners?.forEach { it(this) }
    }

    /**
     * Add the specified Tweak as a child to this one.
     * Returns the added Tweak.
     */
    fun <C: Any>add(child: Tweak<C>): Tweak<C> {
        // Checks
        if (childrenEditable.contains(child)) throw IllegalArgumentException("The tweak '${child.name}' is already contained in '${this.name}'!")
        if (child == this) throw IllegalArgumentException("Can't add a tweak to itself!")
        if (hasParentage(child)) throw IllegalArgumentException("Can't add a tweak to one of it's children or grandchildren!")

        // Remove any earlier tweak with the same name
        remove(child.name)

        // Remove from old parent
        child.parent?.remove(child)

        // Add to this
        child.parent = this
        children.add(child)

        return child
    }

    /**
     * Convenience method for building structures of tweaks.
     * Takes a builder as last argument with the tweak as receiver, which means that add() can be followed by
     * a block { } that can assign values or add children to the new tweak directly (as if it was 'this' in that block).
     *
     * E.g.
     * add("plant", Branch()) {
     *     add("branch_count", 10)
     *     add("branch_generator", Branch()) {
     *         add("thickness", 2.0)
     *         add("color", Color(100, 50, 20))
     *         add("tip_generator", Flower()) {
     *             add("petal_count", 10.0)
     *             add("color", Color(200, 80, 120))
     *         }
     *     }
     * }
     */
    fun <C: Any>add(
        name: String,
        initialValue: C,
        description: String? = null,
        updater: TweakFun<C>? = null,
        type: Class<C> = initialValue.javaClass,
        builder: Tweak<C>.() -> Unit = {}
    ): Tweak<C> {
        val tweak = add(Tweak(type, name.toSymbol(), initialValue, description, updater))
        tweak.builder()
        return tweak
    }

    /**
     * Remove the specified child, if it is contained in this Tweak.
     * @return true if removed.
     */
    fun remove(child: Tweak<*>): Boolean {
        val removed = childrenEditable.remove(child)
        if (removed) child.parent = null
        return removed
    }

    /**
     * Remove the child with the specified name, if any
     * @return true if removed.
     */
    fun remove(childName: Symbol): Boolean {
        val child = get(childName)
        return if (child != null) remove(child) else false
    }

    fun removeAllChildren() {
        while (childrenEditable.isNotEmpty()) {
            remove(childrenEditable.last())
        }
    }

    /**
     * Move the specified child of this tweak to the specified index.
     * Throws exception if the index is out of bounds.
     */
    fun move(child: Tweak<*>, newIndex: Int) {
        childrenEditable.move(child, newIndex)
    }

    /**
     * Add a listener that will be called after each update call for this tweak.
     */
    fun addUpdateListener(listener: (Tweak<T>) -> Unit) {
        if (updateListeners == null) {
            updateListeners = ArrayList()
        }
        updateListeners!!.add(listener)
    }

    /**
     * Remove the specified update listener if found.
     */
    fun removeUpdateListener(listener: (Tweak<T>) -> Unit) {
        updateListeners?.remove(listener)
    }

    /**
     * Should add the source code for this tweak, it's initializer and updater, and its children to the specified builder.
     */
    // TODO: Any custom types used should support serialization to a constructor function call of other types..
    override fun buildCode(builder: CodeBuilder) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     * Saves this tweak to a string using the specified serializer.
     * Currently uses the JSON serializer by default.
     */
    fun save(serializer: TweakSerializer = JsonTweakSerializer): String = serializer.save(this)

    /**
     * Saves this tweak to a file using the specified serializer.
     * Currently uses the JSON serializer by default.
     */
    fun save(outputFile: File, serializer: TweakSerializer = JsonTweakSerializer) = serializer.save(this, outputFile)

    /**
     * Saves this tweak to a writer using the specified serializer.
     * Currently uses the JSON serializer by default.
     */
    fun save(writer: Writer, serializer: TweakSerializer = JsonTweakSerializer) = serializer.save(this, writer)

    /**
     * Saves this tweak to an output stream using the specified serializer.
     * Currently uses the JSON serializer by default.
     */
    fun save(outputStream: OutputStream, serializer: TweakSerializer = JsonTweakSerializer) = serializer.save(this, outputStream)

    companion object {
        /**
         * Load a tweak from a string using the specified serializer.
         * Throws an exception if a tweak could not be loaded from the input.
         * Currently uses the JSON serializer by default.
         */
        fun load(input: String, serializer: TweakSerializer = JsonTweakSerializer): Tweak<*> = serializer.load(input)

        /**
         * Load a tweak from a file using the specified serializer.
         * Throws an exception if a tweak could not be loaded from the input.
         * Currently uses the JSON serializer by default.
         */
        fun load(input: File, serializer: TweakSerializer = JsonTweakSerializer): Tweak<*> = serializer.load(input)

        /**
         * Load a tweak from a reader using the specified serializer.
         * Throws an exception if a tweak could not be loaded from the input.
         * Currently uses the JSON serializer by default.
         */
        fun load(input: Reader, serializer: TweakSerializer = JsonTweakSerializer): Tweak<*> = serializer.load(input)

        /**
         * Load a tweak from a stream using the specified serializer.
         * Throws an exception if a tweak could not be loaded from the input.
         * Currently uses the JSON serializer by default.
         */
        fun load(input: InputStream, serializer: TweakSerializer = JsonTweakSerializer): Tweak<*> = serializer.load(input)

    }
}