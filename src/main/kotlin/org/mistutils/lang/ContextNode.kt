package org.mistutils.lang

import org.mistutils.symbol.Symbol

/**
 * An abstract way to refer to tweaks, allowing integration with other structures a bit easier if needed.
 * Only has methods for reading / accessing fields in tweaks, not for modifying them.
 */
interface ContextNode<T> {
    val name: Symbol
    val type: Class<T>
    val value: T
    val description: String?

    val parent: ContextNode<*>?
    val children: List<ContextNode<*>>

    val root: ContextNode<*> get() = parent?.root ?: this
    val isRoot: Boolean get() = parent == null

    operator fun get(name: Symbol): ContextNode<*>?
    operator fun get(ref: Ref): ContextNode<*>?

}