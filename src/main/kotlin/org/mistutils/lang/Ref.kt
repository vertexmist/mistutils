package org.mistutils.lang

import org.mistutils.strings.toSymbol
import org.mistutils.symbol.Symbol

/**
 * A path to a [Tweak].  Can be relative or start from the root.
 */
data class Ref(val head: Symbol?,
               val tail: Ref?,
               val fromRoot: Boolean = false) {

    constructor(path: List<Symbol>, fromRoot: Boolean = false): this(path.firstOrNull(), if (path.size >= 2) Ref(
        path.drop(1)
    ) else null, fromRoot)

    constructor(path: String): this(path.removePrefix("/").split("/").map { it.toSymbol() }, path.startsWith("/"))

}