package org.mistutils.lang.tweakfun.functions

import org.mistutils.lang.tweakfun.TweakNum2Fun


class AddFun: TweakNum2Fun("add", "Adds two numbers together.", calc = { a, b -> a + b })
class SubFun: TweakNum2Fun("sub", "Subtracts b from a.", calc = { a, b -> a - b })
class MulFun: TweakNum2Fun("mul", "Multiplies two numbers together.", 1.0, calc = { a, b -> a * b })
class DivFun: TweakNum2Fun("div", "Divides a with b.", 1.0, calc = { a, b -> a / b })

