package org.mistutils.lang.tweakfun

import org.mistutils.lang.ContextNode
import org.mistutils.strings.toSymbol
import org.mistutils.symbol.Symbol

data class FunParam<T: Any>(
    val name: Symbol,
    val initialValue: () -> T,
    val type: Class<T> = initialValue().javaClass,
    val description: String? = null) {

    constructor(name: String,
                initialValue: () -> T,
                type: Class<T> = initialValue().javaClass,
                description: String? = null) : this(name.toSymbol(), initialValue,  type, description)

    fun getValue(context: ContextNode<*>): T {
        val paramNode = context[name]
        return if (paramNode != null && type.isAssignableFrom(paramNode.type)) {
            paramNode.value as T
        } else {
            // Should not happen if the function node has been correctly initialized
            initialValue()
        }
    }
}