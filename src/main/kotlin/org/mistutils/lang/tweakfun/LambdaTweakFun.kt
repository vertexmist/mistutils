package org.mistutils.lang.tweakfun

import org.mistutils.lang.ContextNode
import org.mistutils.lang.Ref
import org.mistutils.symbol.Symbol

/**
 * Implementation of TweakFun that takes the calculation as a parameter.
 */
class LambdaTweakFun<T>(
    override val id: Ref,
    override val type: Class<T>,
    override val description: String? = null,
    override val parameters: List<FunParam<*>>,
    val calc: (context: ContextNode<T>, parameters: Map<Symbol, *>) -> T
): TweakFun<T> {
    override fun calculate(context: ContextNode<T>, parameters: Map<Symbol, *>): T {
        return calc(context, parameters)
    }
}