package org.mistutils.lang.tweakfun

import org.mistutils.lang.ContextNode

/**
 * Numerical function with two operands.
 */
open class TweakNum2Fun(name: String,
                        description: String? = null,
                        initialValueA: Double = 0.0,
                        initialValueB: Double = initialValueA,
                        calc: ContextNode<Double>.(a: Double, b: Double) -> Double): TweakFun2<Double, Double, Double>(
    name,
    Double::class.java,
    description,
    FunParam("a", {initialValueA}, Double::class.java),
    FunParam("b", {initialValueB}, Double::class.java),
    calc
)