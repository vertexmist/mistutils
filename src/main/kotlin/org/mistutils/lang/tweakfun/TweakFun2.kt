package org.mistutils.lang.tweakfun

import org.mistutils.lang.ContextNode
import org.mistutils.lang.Ref
import org.mistutils.strings.toSymbol

/**
 * Function with specified number of parameters.
 */
open class TweakFun2<P1: Any, P2: Any, T: Any>(
    name: String,
    override val type: Class<T>,
    override val description: String? = null,
    val p1: FunParam<P1>,
    val p2: FunParam<P2>,
    val calc: ContextNode<T>.(P1, P2) -> T
): TweakFunBase<T>() {

    override val id: Ref = Ref(name.toSymbol(), null, true)
    override val parameters: List<FunParam<*>> = listOf(p1, p2)

    override fun calculate(context: ContextNode<T>): T {
        return context.calc(
            p1.getValue(context),
            p2.getValue(context))
    }
}