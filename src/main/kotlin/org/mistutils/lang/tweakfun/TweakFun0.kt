package org.mistutils.lang.tweakfun

import org.mistutils.lang.ContextNode
import org.mistutils.lang.Ref
import org.mistutils.strings.toSymbol

/**
 * Function with specified number of parameters.
 */
open class TweakFun0<T: Any>(
    name: String,
    override val type: Class<T>,
    override val description: String? = null,
    val calc: ContextNode<T>.() -> T
): TweakFunBase<T>() {

    override val id: Ref = Ref(name.toSymbol(), null, true)
    override val parameters: List<FunParam<*>> = listOf()

    override fun calculate(context: ContextNode<T>): T {
        return context.calc()
    }
}