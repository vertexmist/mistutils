package org.mistutils.lang.tweakfun

import org.mistutils.lang.ContextNode
import org.mistutils.lang.Ref
import org.mistutils.symbol.Symbol

/**
 * Defines a function to calculate a value from a given list of parameters.
 */
interface TweakFun<T> {

    val id: Ref

    val type: Class<T>

    val description: String?

    val parameters: List<FunParam<*>>

    fun calculate(context: ContextNode<T>): T {
        val map = HashMap<Symbol, Any?>()

        for (parameter in parameters) {
            map[parameter.name] = parameter.getValue(context)
        }

        return calculate(context, map)
    }

    fun calculate(context: ContextNode<T>,
                  parameters: Map<Symbol, *>): T

}