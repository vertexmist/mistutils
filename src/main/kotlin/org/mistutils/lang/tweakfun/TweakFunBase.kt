package org.mistutils.lang.tweakfun

import org.mistutils.lang.ContextNode
import org.mistutils.symbol.Symbol

/**
 *
 */
abstract class TweakFunBase<T>: TweakFun<T> {

    override fun calculate(context: ContextNode<T>, parameters: Map<Symbol, *>): T {
        throw IllegalArgumentException("Not implemented")
    }
}