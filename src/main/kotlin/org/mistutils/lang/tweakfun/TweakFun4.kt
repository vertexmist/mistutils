package org.mistutils.lang.tweakfun

import org.mistutils.lang.ContextNode
import org.mistutils.lang.Ref
import org.mistutils.strings.toSymbol

/**
 * Function with specified number of parameters.
 */
open class TweakFun4<P1: Any, P2: Any, P3: Any, P4: Any, T: Any>(
    name: String,
    override val type: Class<T>,
    override val description: String? = null,
    val p1: FunParam<P1>,
    val p2: FunParam<P2>,
    val p3: FunParam<P3>,
    val p4: FunParam<P4>,
    val calc: ContextNode<T>.(P1, P2, P3, P4) -> T
): TweakFunBase<T>() {

    override val id: Ref = Ref(name.toSymbol(), null, true)
    override val parameters: List<FunParam<*>> = listOf(p1, p2, p3, p4)

    override fun calculate(context: ContextNode<T>): T {
        return context.calc(
            p1.getValue(context),
            p2.getValue(context),
            p3.getValue(context),
            p4.getValue(context))
    }
}