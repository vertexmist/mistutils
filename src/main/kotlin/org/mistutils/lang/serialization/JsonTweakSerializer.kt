package org.mistutils.lang.serialization

import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Klaxon
import org.mistutils.classes.toNumberOfType
import org.mistutils.lang.Tweak
import org.mistutils.lang.tweakfun.TweakFun
import org.mistutils.strings.isStrictIdentifier
import org.mistutils.strings.toSymbol
import org.mistutils.lang.NoValue
import tornadofx.FX
import java.io.Reader
import java.io.StringReader

object JsonTweakSerializer: TweakSerializer {


    override fun load(input: String): Tweak<*> {
        return load(StringReader(input))
    }

    override fun load(reader: Reader): Tweak<*> {
        val klaxon = Klaxon()
        val wrapper: JsonObject = klaxon.parseJsonObject(reader)
        return parseTweak(wrapper.obj("tweak") ?: throw IllegalArgumentException("tweak-entry not found"))
    }

    override fun save(tweak: Tweak<*>): String {
        val obj = toJsonObject(tweak)
        val wrapper = JsonObject()
        wrapper["format_type"] = "ParamTweak_data"
        wrapper["format_version"] = "1.0"
        wrapper["format_meta_info"] =
            "Data file for the ParamTweak application, " +
                    "used for defining hierarchical parameter data " +
                    "that may change over time or based on input parameters. " +
                    "(TODO: Insert url here)"
        wrapper["tweak"] = obj
        return wrapper.toJsonString(true)
    }


    private fun toJsonObject(tweak: Tweak<*>): JsonObject {
        val obj = JsonObject()
        obj["name"] = tweak.name
        if (tweak.description != null) obj["desc"] = tweak.description
        if (tweak.type != NoValue::class.javaObjectType) obj["type"] = serializeType(tweak.type)
        if (tweak.value != NoValue) {
            val serializedValue = serializeValue(tweak.value)
            if (serializedValue != null) {
                obj["value"] = serializedValue
            }
        }
        if (tweak.updater?.id != null) obj["fun"] = tweak.updater?.id.toString()
        if (tweak.children.isNotEmpty()) {
            val cs = JsonArray<JsonObject>()
            for (child in tweak.children) {
                cs.add(toJsonObject(child))
            }
            obj["children"] = cs
        }
        return obj
    }




    private fun parseTweak(obj: JsonObject): Tweak<*> {
        // Read tweak fields
        val name = obj.string("name") ?: "Unnamed".also{ FX.log.warning("Missing name for loaded Tweak, renaming it 'Unnamed'")}
        val desc = obj.string("desc")
        val type: Class<Any> = parseType(obj.string("type")) as Class<Any>
        val value = parseValue(obj, "value", type)
        val function = obj.string("fun")

        // Integrity check
        if (value != NoValue && !type.isInstance(value)) throw IllegalArgumentException("Value of tweak '$name' is '$value' of type ${value.javaClass}, but type specified as $type")
        if (!name.isStrictIdentifier()) throw IllegalArgumentException("Tweak '$name' has invalid name, should start with ascii letter or underscore and contain only ascii letters, underscores and digits.")

        // Create tweak
        // TODO: Get function instance, use function to look it up
        val updater: TweakFun<Any>? = null
        val tweak = Tweak(type, name.toSymbol(), value, desc, updater)

        // Parse children
        val children = obj.array<JsonObject>("children")
        if (children != null) {
            for (child in children) {
                tweak.add(parseTweak(child))
            }
        }

        return tweak
    }

    private fun serializeType(type: Class<*>): String {
        // return TweakFuns.getTypeName(type) ?:
        throw IllegalArgumentException("Class '${type.name}' not supported")
    }

    private fun parseType(typeName: String?): Class<*> {
        return if (typeName.isNullOrBlank()) NoValue::class.java
        else {
            //  TweakFuns.getTypeForName(typeName) ?:
            throw IllegalArgumentException("Class '$typeName' not found")
        }
    }

    private fun serializeValue(value: Any): Any? {
        return if (value is Number || value is String || value is Boolean) {
            value
        }
        else {
            null/*
                val serializer = TweakFuns.typeSerializers[value.javaClass]
                if (serializer != null) serializer(value)
                else null */
        }
    }

    private fun parseValue(
        obj: JsonObject,
        fieldName: String,
        type: Class<Any>
    ): Any {
        val value: Any? = obj[fieldName]
        return when (type) {
            NoValue::class -> NoValue
            Double::class.javaObjectType -> (value as Number).toDouble()
            Boolean::class.javaObjectType -> value as Boolean
            String::class.javaObjectType -> value.toString()
            else -> {
                if (Number::class.javaObjectType.isAssignableFrom(type)) {
                    // We want the value as some other numerical type, convert
                    (value as Number).toNumberOfType(type as Class<out Number>)
                }

                // TODO: Support other types?
                /*
                val deSerializer = TweakFuns.typeDeSerializers[type]
                if (deSerializer != null) {
                    deSerializer(value.toString())
                }
                else */ {
                    throw IllegalArgumentException("Unsupported value type: $type")
                }
            }
        }
    }

}