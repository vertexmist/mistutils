package org.mistutils.lang.serialization

import org.mistutils.lang.Tweak

object TweakLangSerializer: TweakSerializer {
    override fun load(input: String): Tweak<*> {
        TODO("parse")
    }

    override fun save(tweak: Tweak<*>): String {
        return tweak.toCode()
    }
}