package org.mistutils.lang.serialization

import org.mistutils.lang.Tweak
import java.io.*

/**
 * Provides some means to save and load tweaks.
 */
interface TweakSerializer {

    fun load(input: String): Tweak<*>
    fun load(reader: Reader): Tweak<*> = reader.use { load(it.readText()) }
    fun load(inputStream: InputStream): Tweak<*> = inputStream.use{ load(InputStreamReader(it, Charsets.UTF_8)) }
    fun load(file: File): Tweak<*> = load(file.readText(Charsets.UTF_8))

    fun save(tweak: Tweak<*>): String
    fun save(tweak: Tweak<*>, writer: Writer) = writer.use { it.write(save(tweak)) }
    fun save(tweak: Tweak<*>, outputStream: OutputStream) = outputStream.writer(Charsets.UTF_8).use { save(tweak, it) }
    fun save(tweak: Tweak<*>, file: File) = file.writeText(save(tweak), Charsets.UTF_8)
}