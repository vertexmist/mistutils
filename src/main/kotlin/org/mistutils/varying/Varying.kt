package org.mistutils.varying

/**
 * Something that can be operated on and passed around, but
 * has its value calculated on the fly on every request,
 * enabling continuously changing values at the cost of calculating
 * it every time.
 *
 * Basically a bit like a numerical abstract syntax tree node.
 * Could be used as an AST node?
 */
interface Varying {
    val value: Double

    operator fun plus(other: Varying) = VaryingOp(this, other) { a, b -> a + b}
    operator fun minus(other: Varying) = VaryingOp(this, other) { a, b -> a - b}
    operator fun times(other: Varying) = VaryingOp(this, other) { a, b -> a * b}
    operator fun div(other: Varying) = VaryingOp(this, other) { a, b -> a / b}

    operator fun plus(other: Double) = plus(VaryingValue(other))
    operator fun minus(other: Double) = minus(VaryingValue(other))
    operator fun times(other: Double) = times(VaryingValue(other))
    operator fun div(other: Double) = div(VaryingValue(other))

    fun map(operation: (Double) -> Double): Varying = VaryingFun(this, operation)
    fun combine(other: Varying, operation: (Double, Double) -> Double): Varying = VaryingOp(this, other, operation)

}


operator fun Double.plus(other: Varying) = VaryingOp(VaryingValue(this), other) { a, b -> a + b}
operator fun Double.minus(other: Varying) = VaryingOp(VaryingValue(this), other) { a, b -> a - b}
operator fun Double.times(other: Varying) = VaryingOp(VaryingValue(this), other) { a, b -> a * b}
operator fun Double.div(other: Varying) = VaryingOp(VaryingValue(this), other) { a, b -> a / b}

fun Double.toVarying() = VaryingValue(this)
