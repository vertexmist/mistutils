package org.mistutils.varying

/**
 * Combines two varying in some way.
 */
class VaryingOp(val a: Varying,
                val b: Varying,
                val op: (a: Double, b: Double) -> Double): Varying {
    override val value: Double get() = op(a.value, b.value)
}