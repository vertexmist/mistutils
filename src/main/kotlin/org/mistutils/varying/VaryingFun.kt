package org.mistutils.varying

/**
 * A Varying that applies an operation to another varying and returns the result.
 */
class VaryingFun(val v: Varying,
                 val op: (Double) -> Double): Varying {

    override val value: Double get() = op(v.value)
}