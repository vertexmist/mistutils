package org.mistutils.color.colortype

import javafx.scene.paint.Color
import org.mistutils.geometry.double3.Double3
import org.mistutils.geometry.double3.MutableDouble3

/**
 * JavaFX specific color utilities.
 */
object FxColorType: ColorType<Color>() {

    fun getRed(color: Color): Double = color.red
    fun getGreen(color: Color): Double = color.green
    fun getBlue(color: Color): Double = color.blue
    override fun getOpacity(color: Color): Double = color.opacity

    override fun getColorValues(color: Color, out: MutableDouble3): Double3 {
        out.x = getRed(color)
        out.y = getGreen(color)
        out.z = getBlue(color)
        return out
    }

    override fun createColorRGB(red: Double, green: Double, blue: Double, alpha: Double, colorOut: Color?): Color {
        // JavaFX colors are immutable, so create a new one ignoring colorOut
        return Color(red, green, blue, alpha)
    }
}