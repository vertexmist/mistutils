package org.mistutils.geometry.int3

import org.mistutils.geometry.int2.Int2

/**
 *
 */
class ImmutableInt3(override val x: Int = 0,
                    override val y: Int = 0,
                    override val z: Int = 0): Int3Base() {

    constructor(other: Int3): this(other.x, other.y, other.z)
    constructor(other: Int2, z: Int = 0): this(other.x, other.y, z)

}