package org.mistutils.metrics

import org.mistutils.random.RandomSequence
import kotlin.concurrent.thread

fun main() {
    // Create metrics and metrics view
    val metrics = DefaultMetrics("Global Warming and the Pirate - Orc conundrum")
    metrics.showView()

    // Show two UIs to demonstrate that javafx starting works
    val metrics2 = DefaultMetrics("Performance")
    metrics2.showView()
    metrics2.showView() // Second call to showView() should not create a new view.

    // Show example data
    val random = RandomSequence.createDefault()
    var pirates = 100.0
    var orcs = 33.0
    var globalWarming = 0.0
    thread(isDaemon = true) {
        while (true) {
            // Record some metrics
            metrics.report("pirates", pirates)
            metrics.report("orcs", orcs)
            metrics.report("globalWarming", globalWarming)
            metrics.report("alarmingTemperature", globalWarming > 1.0)

            pirates += random.nextGaussian(-0.1 * orcs, 0.01)
            orcs += random.nextGaussian(pirates * 0.01 - globalWarming * 0.01, 0.01) * 0.1
            globalWarming += random.nextGaussian(-pirates * 0.02, 0.01) * 0.1

            // FPS
            metrics2.reportFrameTime(frameTimeMilliseconds = true)

            // Memory
            metrics2.reportMemoryUsage()

            Thread.sleep(100)
        }
    }
}