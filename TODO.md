# TODO Items


## Refactorings

### RandomSequence
* Rename to something shorter
* Rename nextXxx to xxx
* Unify random hash and random generator? - if random hash can be given a seed then they could be similar.
  * But there is utility in being able to pass in a seed and getting it hashed
  
### Varying
* Could it be renamed to Var:s?  Too confusing?
  
### Tweaks
* Any unecessary work that could be removed?  Unify with Varyings/Vars
  

----------------------------  
## Optimize
* Optimize HSLuv conversions, seems like a lot of nested computations used in the external library, could probably crunch it down.



----------------------------  
## Create

### Space3D / voxel space / chunks
* An efficient voxel space system
  * perhaps as own library? - on the other hand, noises are here?
  * generator function + edit deltas
  * generate visible/nearby & cache them
  * store edits as deltas
  * save edits to file
  * Level of detail chunks, generator can skip high detail layers for faraway chunks?
  * Save edit deltas as mipmapped structure? - apply specific delta level to specific detail level
  * multiple materials?
  * material amounts / densities?
  * fluid simulation? - maintain steady-state flow information, update once in a while, or when system interfered with?
    * reservoirs and streams etc, with inflow, outflow, how much more volume can fit before new flow will be calculated, etc?
    
        
