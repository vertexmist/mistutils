# Mistutils

This is a utility library for making my, and perhaps your, coding easier, 
faster, and more enjoyable.  It's written mostly in Kotlin, but should
be more or less usable for any Java Virtual Machine -based language.

It contains a broad range of small utility classes and kotlin extension
functions for existing classes.


## License

Licensed under LGPL v.3, so it is usable in commercial projects as well.


## Reporting Issues

Please report any bugs or feature requests to the issue tracker at
https://gitlab.com/vertexmist/mistutils/issues

## Setup

You can use jitpack to get a maven dependency for this project for now:
[![](https://jitpack.io/v/com.gitlab.vertexmist/mistutils.svg)](https://jitpack.io/#com.gitlab.vertexmist/mistutils)


## Usage

In general all classes and functions have java/kotlin docs.
 